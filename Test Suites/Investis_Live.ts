<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Investis_Live</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>cf8d1aa8-ddb7-4b4e-b2d3-57a741ac6e81</testSuiteGuid>
   <testCaseLink>
      <guid>de228548-5873-4360-9edd-24d700cd0995</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login and Forgot Password</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>45ff34fe-0389-46be-a644-f5ee2682517b</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/data</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>45ff34fe-0389-46be-a644-f5ee2682517b</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Username</value>
         <variableId>4adfdc53-0a97-4627-ae71-279d409c426b</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>45ff34fe-0389-46be-a644-f5ee2682517b</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>45f0b989-4f2a-45e7-8641-12aba9c187a7</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>319f0848-938f-4de2-9b3c-c69a08d89c4c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create_Client</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>32e4d0d4-2845-4da4-b659-36e409a95a15</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Analytics_Module</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>40ede606-4586-4e1e-8c26-a2e33358b9e0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create_Presentation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6c960782-d64b-40a1-8355-8a0a1161079a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Reporting View_Download</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7f2c9667-1629-47f7-b14d-645635d6eb0a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Edit_Presentation_With_Assets</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>637955de-8f02-48c0-a4f4-2c27070cf9ba</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Set_Prelive</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>66135f1c-fe25-4de3-8fa9-d0d4fa9c132e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/UserSide_Presentation_Live_Prelive</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8e5cd5df-be41-4b5a-a3d7-d1bec22a791c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login_Admin</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>67fbfbf1-0ac5-465f-be7b-1eea2ea6cecc</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ba98ebfa-06c3-4e40-92ca-7757fdcf4b5f</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>07990ed7-bedb-4f4e-9c50-260d9b32e5dc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Presentation_List</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6c48c12c-df21-4d09-8950-1917068e895c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Set_Live</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>09afa67d-bfdb-4b58-b101-8566519171bd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/UserSide_Presentation_Live_Live</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>08cdfcb4-1342-481b-a3cb-9bc9c0dc21eb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login_Admin</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>67fbfbf1-0ac5-465f-be7b-1eea2ea6cecc</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ba98ebfa-06c3-4e40-92ca-7757fdcf4b5f</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>dd86f4ac-682e-4b96-a1fd-c3b92fb2dbb6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Presentation_List</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b371aad2-0f2b-4692-be4e-8251fce76eae</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Set_Ondemand</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d561b330-e25e-4569-a331-5dd7519522a4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/UserSide_Presentation_Live_Ondemand</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>22a2a11f-8af7-4e54-9236-265238396a06</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login_Admin</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>67fbfbf1-0ac5-465f-be7b-1eea2ea6cecc</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ba98ebfa-06c3-4e40-92ca-7757fdcf4b5f</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>98cdc2ee-c19a-4bbc-b051-657e426fb7c8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Presentation_List</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>750bacef-4507-494d-bf30-30ba7877d01b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Set_Postlive</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ead73a29-ab5c-487d-a486-480f9ff44afd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/UserSide_Presentation_Live_Postlive</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8c02f18a-46e9-440d-aeb0-f8f03ac528cc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Data_Deletion_After_Test</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>660b0c0e-ccfd-4b21-b306-a413596ca7ab</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>08154b5d-374f-4dc4-b8f7-569742c373ad</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>

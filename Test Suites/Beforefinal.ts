<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Beforefinal</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>0fcdf823-1eec-40a5-b880-a19d33a47582</testSuiteGuid>
   <testCaseLink>
      <guid>4a38923a-bbb6-45d6-98fe-afb15228bd6c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Presentation_List</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6e7df41f-007b-4338-acaf-fe212c3b3938</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Set_Prelive</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e09e265c-f6da-4df8-9503-42e1f3fca26e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/UserSide_Presentation_Live_Prelive</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5b7a3c37-aeda-4d1b-9c40-b6d5c5283140</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login_Admin</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>0d592d1f-7f4b-43f9-a157-11563d79c265</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/data</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>0d592d1f-7f4b-43f9-a157-11563d79c265</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Username</value>
         <variableId>67fbfbf1-0ac5-465f-be7b-1eea2ea6cecc</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>0d592d1f-7f4b-43f9-a157-11563d79c265</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>ba98ebfa-06c3-4e40-92ca-7757fdcf4b5f</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>4b269032-a418-4cfd-8090-fc1b4d80a481</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Presentation_List</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d417df9d-39b7-40fe-8939-346f7961c97b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Set_Live</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>64402817-d543-4f4a-8e92-1d2c36ef9bb8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/UserSide_Presentation_Live_Live</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2e79fdd5-6f75-4b8b-b072-9acb11e609f0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login_Admin</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>ecb88320-6762-436f-9ee7-385b74cac3ed</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/data</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>ecb88320-6762-436f-9ee7-385b74cac3ed</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Username</value>
         <variableId>67fbfbf1-0ac5-465f-be7b-1eea2ea6cecc</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ecb88320-6762-436f-9ee7-385b74cac3ed</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>ba98ebfa-06c3-4e40-92ca-7757fdcf4b5f</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>7e3e79d2-8300-4645-aafd-70bc800d08a0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Presentation_List</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0182c459-5221-469c-8c86-7322c63eaaac</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Set_Ondemand</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e9def9a3-4661-4673-8544-2d3e2982a3a4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/UserSide_Presentation_Live_Ondemand</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>44596961-c81d-4ffe-bc22-2c22c6f69d3d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login_Admin</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>7a8949c4-280e-4bfc-a4c3-f4879db629c1</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/data</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>7a8949c4-280e-4bfc-a4c3-f4879db629c1</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Username</value>
         <variableId>67fbfbf1-0ac5-465f-be7b-1eea2ea6cecc</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>7a8949c4-280e-4bfc-a4c3-f4879db629c1</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>ba98ebfa-06c3-4e40-92ca-7757fdcf4b5f</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>dccc9803-7149-47cb-b1ac-f8f174a33348</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Presentation_List</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2c7c45c2-6b7d-47a5-a2ea-4415171c4789</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Set_Postlive</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9725ad31-0de9-447c-a6fb-b7b6a0921acd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/UserSide_Presentation_Live_Postlive</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a2f018a6-1255-4f66-ab0c-f540082a4adc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Data_Deletion_After_Test</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>660b0c0e-ccfd-4b21-b306-a413596ca7ab</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>08154b5d-374f-4dc4-b8f7-569742c373ad</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>

<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Video_Players</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>a43df07f-c066-4236-a583-7c4d984a93a9</testSuiteGuid>
   <testCaseLink>
      <guid>96e67da8-ca65-4985-b6ac-55987a1f7092</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Video_Player_Self-Hosted-Video</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8eb407d2-4efb-462d-bbca-9d60b3b2f19c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Video_Player_Youtube</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c728ef96-1e1e-4237-a52a-64a8a8d04d33</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Video_Player_Brightcove</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5222bc91-9924-44df-833e-7aa3a7939810</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Video_Player_Closed Captions Multilingual</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>482f53f6-38c6-4b87-95b8-041e9ea70408</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Video_Player_HLS Adaptive</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>42623eec-6088-463a-9a1d-efcbd672a76e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Video_Player_HLS Live Adaptive</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0276a835-8401-46ab-a4f4-f0b99efde46e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Video_Player_HLS Live</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fb557ece-6451-4d3c-a072-c782ea603fb1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Video_Player_Accessibility</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e589bbb4-8ef5-42bd-a0a1-9260d8204da3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Video_Player_JavaScript_Disabled</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0d6b21c8-cda7-44a8-8cf9-ff39490320cb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Video_Player_Social_Share</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a1633e7c-e641-4f9b-a8ef-87cc29c20083</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Video_Player_Custom_Skin</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1d81f074-f359-4bca-8140-fb341ccafc24</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Video_Player_Vimeo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ab70350d-0d2b-4b3b-87b2-e0af28603e36</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Video_Player_Popup_Player</testCaseId>
   </testCaseLink>
</TestSuiteEntity>

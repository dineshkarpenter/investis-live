import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Investis Digital'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Investis Digital_1'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Calendar'))

WebUI.takeScreenshot('Data Files/Screenshots/calendar_upcoming.png')

WebUI.click(findTestObject('Object Repository/Page_Investis Live/h3_Upcoming Presentations'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Previous'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/h3_Previous Presentations'))

WebUI.doubleClick(findTestObject('Object Repository/Page_Investis Live/h3_Previous Presentations'))

WebUI.takeScreenshot('Data Files/Screenshots/calendar_previous.png')

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Investis Digital'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Investis Digital_1'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Q'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_QA_cliento1'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Playlist'))

WebUI.takeScreenshot('Data Files/Screenshots/presentation_playlist.png')

WebUI.click(findTestObject('Object Repository/Page_Presentation Editor/a_Go make some'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Preview URLs'))

WebUI.switchToWindowIndex(1)

WebUI.takeScreenshot('Data Files/Screenshots/presentation_preview.png')

WebUI.setText(findTestObject('Object Repository/Page_Viewing qa_presentation001/input_Email_loginemail'), 'qac@mailinator.com')

WebUI.click(findTestObject('Object Repository/Page_Viewing qa_presentation001/input_I have read the terms and conditions _180273'))

WebUI.click(findTestObject('Object Repository/Page_Viewing qa_presentation001/div_New Viewer                    Name     _e819c5'))

WebUI.click(findTestObject('Object Repository/Page_Viewing qa_presentation001/button_Accept'))

WebUI.setText(findTestObject('Object Repository/Page_Viewing qa_presentation001/input_Email_loginemail'), 'qac@mailinator.com')

WebUI.switchToWindowIndex(1)

WebUI.takeScreenshot('Data Files/Screenshots/present_previewwindow.png')

WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/a_Delivered by Investis Digital'))

WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/a_Help'))

WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/a_FAQ'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/a_Contact Us'))

not_run: WebUI.setText(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/input_Name_contactUsname'), 
    'rewr')

not_run: WebUI.setText(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/input_Email_contactUsemail'), 
    'd@mailinator.com')

not_run: WebUI.setText(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/textarea_Message_contactUsmessage'), 
    'test msg')

not_run: WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/input_Type code shown in image above_btnContactUs'))

WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/a_FAQ'))

WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/a_I cannot view the video'))

WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/a_I cannot hear audio'))

WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/a_The slides are not moving'))

WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/a_I am getting an error Cannot load M3U8 cr_d0d08e'))

WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/a_I cannot register'))

WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/a_I have registered before and I cannot sig_e53755'))

WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/a_Presentation keeps stopping and starting'))

WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/a_I have tried on all browsers but still ca_c6e243'))

WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/a_Is there an alternative way to watch the _f05c3b'))

WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/a_Video says server not found'))

WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/a_There is an echo on the audio'))

WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/a_System requirements'))

WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/a_Browser compatibility'))

WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/a_Mobile compatibility'))

WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/a_Cookies'))

WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/a_Socket support'))

WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/a_Video player test'))

WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/video_Video player test_jw-video jw-reset'))

WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/video_Video player test_jw-video jw-reset'))

WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/i_Slides player test_icon-chevron-down pull-right'))

WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/img'))

WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/a_Bandwidth'))


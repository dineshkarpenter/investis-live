import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.navigateToUrl(GlobalVariable.StagingURL)

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Investis Video Player/a_Custom Skin (Theme)'))

WebUI.switchToWindowTitle('Custom Skin')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/button'))

Thread.sleep(GlobalVariable.delaytimeafterstep)

//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/button'))
/*WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/use'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/svg'))
*/
WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/div_mep_1 mejs__playpause-buttonmejs__pause_93d632'))

Thread.sleep(GlobalVariable.delaytimeafterstep)

//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/use_1'))
//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/div_mep_1 mejs__playpause-buttonmejs__pause_93d632_1'))
//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/use_1'))
//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/div_mep_1 mejs__playpause-buttonmejs__pause_93d632_1_2'))
//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/use_1'))
//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/div_mep_1 mejs__playpause-buttonmejs__pause_93d632_1_2_3'))
//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/svg_Chinese_unmute-icon'))
//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/use_1_2'))
//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/button'))
WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/button_1'))

Thread.sleep(GlobalVariable.delaytimeafterstep)

WebUI.takeScreenshot('Data Files/Screenshots/Video-Players-Screenshots/customskin.png')

/*WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/div_000000360137Share                      _b5a68b'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/label_German'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/label_Arabic'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/button_1_2'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/button'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/label_Chinese'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/button_1'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/div_000002000010000030Share                _979ef9'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/button_1'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/div_000002000010000030Share                _979ef9'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/label_German'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/button_1_2_3'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/button_1_2_3_4'))*/
WebUI.closeWindowTitle('Custom Skin')

WebUI.switchToWindowTitle('Investis Video Player')


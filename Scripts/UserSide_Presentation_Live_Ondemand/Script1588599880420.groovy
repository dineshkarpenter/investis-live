import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

/*WebUI.closeWindowIndex(0)

WebUI.switchToWindowIndex(0)*/
WebUI.setText(findTestObject('Page_Viewing QA_CPresentationo1/input_Name_usermetadataname'), 'QAname')

WebUI.setText(findTestObject('Page_Viewing QA_CPresentationo1/input_Company_usermetadatacompany'), 'QAcomp')

WebUI.setText(findTestObject('Page_Viewing QA_CPresentationo1/input_Email_useremail'), 'dinesh.karpenter@investisdigital.com')

WebUI.setText(findTestObject('Page_Investis Live/a_email_password_signin'), 'wPThvGzJ7ekWKPly70yk9Q==')

Thread.sleep(GlobalVariable.shortdelay)

WebUI.check(findTestObject('Page_Viewing QA_CPresentationo1/input_I have read the terms and conditions _180273'))

WebUI.submit(findTestObject('Page_Investis Live/a_viewersignbutton'))

Thread.sleep(GlobalVariable.shortdelay)

WebUI.click(findTestObject('Page_Mailinator  Main Site/a_disclaimer'))

//WebUI.click(findTestObject('Page_Mailinator  Main Site/a_Verify Your Email'))
//
//WebUI.switchToWindowTitle('Mailinator | Main Site')
//
//WebUI.switchToWindowTitle('Viewing "QA_CPresentationo1"')
//
//WebUI.setText(findTestObject('Page_Viewing QA_CPresentationo1/input_Email_loginemail'), 'qaauto1@mailinator.com')
//
//WebUI.click(findTestObject('Page_Viewing QA_CPresentationo1/input_I have read the terms and conditions _180273'))
//
//WebUI.click(findTestObject('Page_Viewing QA_CPresentationo1/html_Viewing QA_CPresentationo1            _145a18'))
WebUI.click(findTestObject('Page_Viewing QA_CPresentationo1 On-demand/button_Video and slides'))

WebUI.click(findTestObject('Page_Mailinator  Main Site/a_playerplay'))

Thread.sleep(GlobalVariable.shortdelay)

WebUI.takeScreenshot('Data Files/Screenshots/Viewer_Ondemand.png')

WebUI.refresh()

//WebUI.click(findTestObject('Page_Investis Live/a_ viewerprofile'))
//
//WebUI.click(findTestObject('Page_Investis Live/a_ viewerprofileclick'))
//
//WebUI.back(FailureHandling.STOP_ON_FAILURE)
//
//Thread.sleep(GlobalVariable.shortdelay)
//
//WebUI.click(findTestObject('Page_Investis Live/a_Live_Audionslides'))
//
//Thread.sleep(GlobalVariable.shortdelay)
//
//WebUI.click(findTestObject('Page_Investis Live/a_Audioplay'))
//
//Thread.sleep(GlobalVariable.shortdelay)
//
//WebUI.takeScreenshot('Data Files/Screenshots/Viewer_ondemand_audio.png')


import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//WebUI.navigateToUrl('https://staging.investis-live.com/investis/clients/qaclient10')

WebUI.navigateToUrl(("$GlobalVariable.StagingURL" + 'investis/clients/') + "$GlobalVariable.client2")

not_run: attribute = WebUI.getAttribute(findTestObject('Object Repository/Page_Investis Live/a_GetLiveURL'), 'value')

not_run: WebUI.openBrowser(attribute)

//WebUI.sendKeys(findTestObject('txt_Comment'), Keys.chord(Keys.CONTROL, 't'))
not_run: println(attribute)

not_run: WebUI.navigateToUrl(attribute)

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Edit'))

Thread.sleep(GlobalVariable.shortdelay)

WebUI.takeScreenshot('Data Files/Screenshots/presentation_editdetails.png')

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Assets'))

Thread.sleep(GlobalVariable.shortdelay)

WebUI.takeScreenshot('Data Files/Screenshots/edit_assets.png')

//WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Media_favicon'))
WebUI.uploadFile(findTestObject('Object Repository/Page_Investis Live/a_Media_favicon'), 'C:\\Users\\dinesh.karpenter\\Katalon Studio\\InvestisLive-Webcasting\\Data Files\\test-photo.jpg')

WebUI.takeScreenshot('Data Files/Screenshots/edit_favicon.png')

//'C:\\Users\\dinesh.karpenter\\test-photo.jpg'
WebUI.uploadFile(findTestObject('Object Repository/Page_Investis Live/a_audioImages'), 'C:\\Users\\dinesh.karpenter\\Katalon Studio\\InvestisLive-Webcasting\\Data Files\\1.jpg')

WebUI.takeScreenshot('Data Files/Screenshots/edit_audioimage.png')

WebUI.uploadFile(findTestObject('Object Repository/Page_Investis Live/a_pdf'), 'C:\\Users\\dinesh.karpenter\\Katalon Studio\\InvestisLive-Webcasting\\Data Files\\SMChart.pdf')

WebUI.scrollToPosition(9999999, 9999999)

Thread.sleep(GlobalVariable.shortdelay)

WebUI.takeScreenshot('Data Files/Screenshots/edit_slidespdf.png')

//WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Streaming'))
//WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Media'))
//WebUI.click(findTestObject('Object Repository/Page_Investis Live/div_Investis Digital     QA_Presentation   _de4038'))
WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Streaming'))

WebUI.takeScreenshot('Data Files/Screenshots/edit_streaming.png')

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Investis Live/select_Audio Only        Video Only        _7cac04'), 
    'video', true)

Thread.sleep(GlobalVariable.shortdelay)

WebUI.takeScreenshot('Data Files/Screenshots/edit_streamselect.png')

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Media'))

WebUI.takeScreenshot('Data Files/Screenshots/edit_media.png')

WebUI.uploadFile(findTestObject('Object Repository/Page_Investis Live/a_audioMediaItem'), 'C:\\Users\\dinesh.karpenter\\Katalon Studio\\InvestisLive-Webcasting\\Data Files\\audio.mp3')

WebUI.waitForElementVisible(findTestObject('Page_Investis Live/a_audiowait'), 30)

WebUI.takeScreenshot('Data Files/Screenshots/edit_audioupload.png')

WebUI.uploadFile(findTestObject('Object Repository/Page_Investis Live/a_videoMediaItem'), 'C:\\Users\\dinesh.karpenter\\Katalon Studio\\InvestisLive-Webcasting\\Data Files\\video.mp4')

WebUI.scrollToPosition(9999999, 9999999)

WebUI.takeScreenshot('Data Files/Screenshots/edit_videoupload.png')

not_run: WebUI.waitForElementVisible(findTestObject('Page_Investis Live/a_videowait'), 1000)

WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Start'))

WebUI.waitForElementVisible(findTestObject('Object Repository/Page_Investis Live/mediaSuccess'), 30)

Thread.sleep(10000)

//The media processing has encountered error. Please Try again. No Video Uploaded
'Verify message after logging in'
not_run: if (WebUI.verifyTextPresent('The media has been processed Successfully.', false)) {
    WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Audience'))
} else {
    WebUI.refresh()

    WebUI.uploadFile(findTestObject('Object Repository/Page_Investis Live/a_audioMediaItem'), 'C:\\Users\\dinesh.karpenter\\Katalon Studio\\InvestisLive-Webcasting\\Data Files\\audio.mp3')

    WebUI.uploadFile(findTestObject('Object Repository/Page_Investis Live/a_videoMediaItem'), 'C:\\Users\\dinesh.karpenter\\Katalon Studio\\InvestisLive-Webcasting\\Data Files\\video.mp4')

    WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Start'))
}

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Audience'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Registration'))

WebUI.takeScreenshot('Data Files/Screenshots/audience_registration.png')

WebUI.click(findTestObject('Page_Investis Live/a_ Audience_predefinedsign'))

WebUI.setText(findTestObject('Page_Investis Live/a_ Audience_predefineduname'), 'dinesh.karpenter@investisdigital.com')

WebUI.setText(findTestObject('Page_Investis Live/a_ Audience_predefinedpass'), 'wPThvGzJ7ekWKPly70yk9Q==')

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Disclaimer'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_Should viewers be shown a disclaimer _f554b9'))

WebUI.takeScreenshot('Data Files/Screenshots/audience_disclaimer.png')

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Notes'))

not_run: WebUI.doubleClick(findTestObject('Object Repository/Page_Investis Live/a_Video Sharing'))

WebUI.takeScreenshot('Data Files/Screenshots/audience_notes.png')

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Video Sharing'))

not_run: WebUI.doubleClick(findTestObject('Object Repository/Page_Investis Live/a_Video Sharing'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Video Sharing'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_Should viewers be able to share the w_701e09'))

not_run: WebUI.doubleClick(findTestObject('Object Repository/Page_Investis Live/input_Should the media be downloadable_pres_80ac14'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_mediadownload'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_Should the media be downloadable_pres_80ac14'))

WebUI.takeScreenshot('Data Files/Screenshots/audience_videosharing.png')

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Questions and Feedback'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_during the presentation_presentationa_5b6c2b'))

WebUI.takeScreenshot('Data Files/Screenshots/audience_notes.png')

not_run: WebUI.doubleClick(findTestObject('Object Repository/Page_Investis Live/input_during the presentation_presentationa_5b6c2b'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_during the presentation_presentationa_5b6c2b'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Cookie Usage Alert'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Viewer Chat'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_Should the viewers be able to chat in_a11044'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_Default Email Template_emailCustomiza_ecf430'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Updateemail'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Pages'))

WebUI.setText(findTestObject('Object Repository/Page_Investis Live/textarea_HTML Content_presentationregistrat_33b33d'), 
    'pages html')

WebUI.takeScreenshot('Data Files/Screenshots/pages_registration.png')

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Disclaimer_1'))

WebUI.setText(findTestObject('Object Repository/Page_Investis Live/textarea_HTML Content_presentationdisclaime_b91eef'), 
    'disclaimer accept html')

WebUI.setText(findTestObject('Object Repository/Page_Investis Live/textarea_Declined content_presentationdecli_f6ee12'), 
    'declined html disclaimer')

WebUI.takeScreenshot('Data Files/Screenshots/pages_disclaimer.png')

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Pre-live'))

WebUI.setText(findTestObject('Object Repository/Page_Investis Live/textarea_HTML Content_presentationprePagecontent'), 'prelive html')

WebUI.takeScreenshot('Data Files/Screenshots/pages_prelive.png')

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Post-live'))

WebUI.setText(findTestObject('Object Repository/Page_Investis Live/textarea_HTML Content_presentationpostPagecontent'), 
    'postlive html')

WebUI.takeScreenshot('Data Files/Screenshots/pages_postlive.png')

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Header'))

not_run: WebUI.doubleClick(findTestObject('Object Repository/Page_Investis Live/a_Header'))

WebUI.setText(findTestObject('Object Repository/Page_Investis Live/textarea_HTML Content_presentationheader'), 'header html')

Thread.sleep(GlobalVariable.shortdelay)

WebUI.takeScreenshot('Data Files/Screenshots/pages_header.png')

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Update'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Tabs'))

WebUI.setText(findTestObject('Object Repository/Page_Investis Live/textarea_Content_contentDownloadscontent'), 'test')

Thread.sleep(GlobalVariable.shortdelay)

WebUI.takeScreenshot('Data Files/Screenshots/tabs.png')

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_ Add another'))

not_run: WebUI.click(findTestObject('Page_Investis Live/button_Update_tab'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Translations'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Fill Default'))

Thread.sleep(GlobalVariable.shortdelay)

WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Download'))

WebUI.takeScreenshot('Data Files/Screenshots/translation.png')

not_run: WebUI.click(findTestObject('Page_Investis Live/button_Update_translation'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Polls'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_Do we need to show results to viewers_bf4cb5'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_ Add new poll'))

WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_Question_pollsPoll 1question'), 'test poll')

WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_Answers_answer span5 valid'), '1')

Thread.sleep(GlobalVariable.shortdelay)

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Update_1'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Polls'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_myInvestis'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_Enable Record Button_presentationclie_2bab32'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_Enable Broadcast Message Button_prese_837fbb'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_Enable Webcast State dropdown_present_470ab2'))

Thread.sleep(GlobalVariable.shortdelay)

WebUI.click(findTestObject('Page_Investis Live/button_Update_myinvestis'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Preview'))

WebUI.switchToWindowIndex(1)

not_run: WebUI.switchToWindowTitle('Viewing "QA-reminderpresentation"')

WebUI.click(findTestObject('Object Repository/Page_Viewing QA-reminderpresentation/em_Select a view to preview'))

WebUI.click(findTestObject('Object Repository/Page_Viewing QA-reminderpresentation/a_Disclaimer'))

WebUI.click(findTestObject('Object Repository/Page_Viewing QA-reminderpresentation/a_Disclaimer_1'))

WebUI.click(findTestObject('Object Repository/Page_Viewing QA-reminderpresentation/a_Pre-live  Visible to viewers'))

WebUI.click(findTestObject('Object Repository/Page_Viewing QA-reminderpresentation/a_Pre-live'))

WebUI.click(findTestObject('Object Repository/Page_Viewing QA-reminderpresentation/a_Live'))

WebUI.click(findTestObject('Page_Investis Live/input_Password_videonslide'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Viewing QA-reminderpresentation/a_Live_1'))

WebUI.click(findTestObject('Page_Viewing QA-reminderpresentation/a_Live - header'))

WebUI.click(findTestObject('Object Repository/Page_Viewing QA_newpresent1/a_Post-live'))

WebUI.click(findTestObject('Object Repository/Page_Viewing QA_newpresent1/a_Post-live_1'))

WebUI.takeScreenshot('Data Files/Screenshots/present_postlive.png')

WebUI.click(findTestObject('Object Repository/Page_Viewing QA_newpresent1/a_On-demand'))

WebUI.click(findTestObject('Page_Investis Live/a_ videonslides_ondemand'))

/*WebUI.click(findTestObject('Object Repository/Page_Viewing QA-reminderpresentation/a_Live'))

WebUI.click(findTestObject('Object Repository/Page_Viewing QA-reminderpresentation/a_Post-live'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Viewing QA-reminderpresentation/a_Post-live_1'))

WebUI.click(findTestObject('Page_Viewing QA-reminderpresentation/a_Pre-live'))
*/
not_run: WebUI.refresh()

WebUI.click(findTestObject('Page_Investis Live/a_Adminback'))


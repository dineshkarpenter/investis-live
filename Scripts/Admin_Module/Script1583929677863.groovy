import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Admin'))

//Access jobs in Admin section 
WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Jobs'))

WebUI.takeScreenshot('Data Files/Screenshots/admin_jobs.png')

//Search jobs with valid keywords
WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input'), 'pro ')

//Thread.sleep(GlobalVariable.myname)
Thread.sleep(GlobalVariable.delaytimeafterstep)

not_run: WebUI.sendKeys(findTestObject('Object Repository/Page_Investis Live/input'), Keys.chord(Keys.ENTER))

WebUI.takeScreenshot('Data Files/Screenshots/admin_jobs_search.png')

//Search jobs with invalid keywords
WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input'), 'que')

WebUI.sendKeys(findTestObject('Object Repository/Page_Investis Live/input'), Keys.chord(Keys.ENTER))

Thread.sleep(GlobalVariable.delaytimeafterstep)

WebUI.takeScreenshot('Data Files/Screenshots/admin_jobs_invalidsearch.png')

//Check jobs with New, processed, completed etc
WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_New 0'))

WebUI.takeScreenshot('Data Files/Screenshots/admin_jobs_new.png')

WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Processing 0'))

WebUI.takeScreenshot('Data Files/Screenshots/admin_jobs_process.png')

WebUI.click(findTestObject('Object Repository/Page_Investis Live/span_72'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Completed 509'))

WebUI.takeScreenshot('Data Files/Screenshots/admin_jobs_completed.png')

WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Total Jobs 585'))

WebUI.takeScreenshot('Data Files/Screenshots/admin_jobs_total.png')

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Admin'))

//Access Users in Admin
WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Users'))

WebUI.takeScreenshot('Data Files/Screenshots/admin_useradd.png')

//Add new  Users in Admin
WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_Name_username'), 'qa')

WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_Company_usercompany'), 'company')

WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_E-mail_useremail'), 'qa87@mailinator.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Investis Live/input_Password_userpassword'), 'aeHFOx8jV/A=')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Investis Live/input_Confirm password_userpassword2'), 'aeHFOx8jV/A=')

WebUI.takeScreenshot('Data Files/Screenshots/admin_user_addform.png')

WebUI.click(findTestObject('Page_Investis Live/input_Password_btn btn-primary - createu'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Delete'))

WebUI.takeScreenshot('Data Files/Screenshots/admin_user_delete.png')

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Admin'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/i_qa87mailinatorcom_icon-pencil'))

WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_Name_username_1'), 'qa2')

WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_E-mail_useremail_1'), 'qa874@mailinator.com')

WebUI.click(findTestObject('Page_Investis Live/input_Password_btn btn-primary - update'))

WebUI.takeScreenshot('Data Files/Screenshots/admin_user_addform1.png')

//Login again
WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_E-mail_loginemail'), GlobalVariable.useremail2)

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Investis Live/input_Password_loginpassword'), GlobalVariable.password21)

WebUI.click(findTestObject('Page_Investis Live/input_Password_btn btn-primary - login'))

//WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Admin'))
WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Admin'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Account'))

WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_Email address_useremail'), 'qa875@mailinator.com')

WebUI.click(findTestObject('Page_Investis Live/input_Password_btn btn-primary - invitep'))

WebUI.takeScreenshot('Data Files/Screenshots/admin_user_addform2.png')

WebUI.click(findTestObject('Page_Investis Live/a_Delete_1_2'))

WebUI.acceptAlert(FailureHandling.STOP_ON_FAILURE)

Thread.sleep(GlobalVariable.shortdelay)

WebUI.takeScreenshot('Data Files/Screenshots/admin_account_delete.png')


import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.interactions.Actions as Actions
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.By as By
import org.openqa.selenium.JavascriptExecutor as JavascriptExecutor
import org.openqa.selenium.PageLoadStrategy as PageLoadStrategy
import org.openqa.selenium.chrome.ChromeDriver as ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions as ChromeOptions

WebUI.navigateToUrl(GlobalVariable.StagingURL)

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Investis Video Player/a_Social Share'))

WebUI.switchToWindowTitle('Social Share')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Social Share/button'))

Thread.sleep(10000)

WebUI.takeScreenshot('Data Files/Screenshots/Video-Players-Screenshots/socialshare.png')

//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Social Share/button_1'))
/*WebUI.click(findTestObject('Object Repository/Video-engine/Page_Social Share/button_Share'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Social Share/div_0000010000000030Share                  _197a22'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Social Share/button_Share'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Social Share/div_0000010000000030Share                  _197a22'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Social Share/button_Share'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Social Share/div_0000010000000030Share                  _197a22'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Social Share/button_Share'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Social Share/div_0000010000000030Share                  _197a22'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Social Share/button_1_2'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Social Share/button_1_2_3'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Social Share/button'))
*/
//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Social Share/button_1_2_3_4'))
//System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
//ChromeOptions options = new ChromeOptions();
//options.setPageLoadStrategy(PageLoadStrategy.NONE);
//Webdriver driver  = new ChromeDriver();
WebUI.mouseOver(findTestObject('Object Repository/Video-engine/Page_Social Share/sharebutton'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Social Share/linkedinshare'))

//WebUI.switchToWindowTitle('Sign Up')
//Thread.sleep(10000)
//WebUI.closeWindowTitle('Sign Up')
//WebUI.switchToWindowTitle('Social Share')
WebUI.closeWindowTitle('Social Share')

WebUI.switchToWindowTitle('Investis Video Player')


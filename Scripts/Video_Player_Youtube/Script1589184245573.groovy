import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.navigateToUrl(GlobalVariable.StagingURL)

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Investis Video Player/a_Youtube'))

WebUI.switchToWindowTitle('Vanilla Player (YouTube)')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (YouTube)/button'))

//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (YouTube)/button'))
//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (YouTube)/button'))
WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (YouTube)/span_000123'))

//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (YouTube)/button'))
WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (YouTube)/button_1'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (YouTube)/button_1_2'))

Thread.sleep(GlobalVariable.delaytimeafterstep)

WebUI.takeScreenshot('Data Files/Screenshots/Video-Players-Screenshots/youtube.png')

//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (YouTube)/button_1_2_3'))
//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (YouTube)/div_Google Privacy Policy_mejs__overlay mej_fc2e65'))
WebUI.closeWindowUrl('https://www.youtube.com/watch?v=YE7VzlLtp-4')

WebUI.closeWindowTitle('Vanilla Player (YouTube)')

WebUI.switchToWindowTitle('Investis Video Player')


import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('https://cdn.tiempodev.com/wp-content/uploads/2019/05/23163136/DevOps-Automation.jpg')

WebUI.maximizeWindow(FailureHandling.STOP_ON_FAILURE)

Thread.sleep(2000)

WebUI.navigateToUrl(GlobalVariable.StagingURL)

//"${GlobalVariable.StagingURL}"+'investis/client'+"${GlobalVariable.useremail2}"
WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Forgot password'))

WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_Enter your e-mail address_email'), 'dineshk@mailinator.com')

WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Send Reset Email'))

WebUI.takeScreenshot('Data Files/Screenshots/Invalid_forgotpass.png')

'Positive forgot password'
WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_Enter your e-mail address_email'), useremail)

WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Send Reset Email'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Login'))

'Negative case'
WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_E-mail_loginemail'), 'webcast@investis.com')

//'webcasting1@investis.com'
WebUI.setEncryptedText(findTestObject('Object Repository/Page_Investis Live/input_Password_loginpassword'), 'jNwsIZtkazw/FxUYNNE+Kw==')

WebUI.click(findTestObject('Page_Investis Live/input_Password_btn btn-primary - login'))

WebUI.takeScreenshot('Data Files/Screenshots/Invalid_login.png')

Thread.sleep(5000)

'Blank Case'
WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_E-mail_loginemail'), '')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Investis Live/input_Password_loginpassword'), '')

WebUI.click(findTestObject('Page_Investis Live/input_Password_btn btn-primary - login'))

WebUI.takeScreenshot('Data Files/Screenshots/Blank_login.png')

Thread.sleep(GlobalVariable.shortdelay)

'Positive Case'
WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_E-mail_loginemail'), GlobalVariable.useremail)

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Investis Live/input_Password_loginpassword'), GlobalVariable.password)

Thread.sleep(GlobalVariable.shortdelay)

//'jNwsIZtkazw/FxUYNNE+Kw=='
WebUI.click(findTestObject('Page_Investis Live/input_Password_btn btn-primary - login'))

WebUI.takeScreenshot('Data Files/Screenshots/Valid_login.png')


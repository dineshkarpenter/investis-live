import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable



WebUI.closeWindowIndex(1)

not_run: WebUI.openBrowser('https://staging.investis-live.com/')

not_run: WebUI.acceptAlert()

not_run: WebUI.callTestCase(findTestCase('Login and Forgot Password'), [:], FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Audience'))

//WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Registration'))
//WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Assets'))
//WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Media'))
//WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Audience'))
not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Update'))

not_run: WebUI.callTestCase(findTestCase('Create_Presentation'), [:], FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Audience'))

//WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Registration'))
//WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Assets'))
//WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Media'))
//WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Audience'))
not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Update'))

not_run: WebUI.openBrowser('')

not_run: WebUI.navigateToUrl('https://staging.investis-live.com/')

not_run: WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_E-mail_loginemail'), 'webcasting@investis.com')

not_run: WebUI.setEncryptedText(findTestObject('Object Repository/Page_Investis Live/input_Password_loginpassword'), 'jNwsIZtkazw/FxUYNNE+Kw==')

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_Password_btn btn-primary'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Q'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_QACLIENT'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Edit_1'))

not_run: WebUI.navigateToUrl('https://staging.investis-live.com/investis/clients/qapresentation')

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Edit'))

not_run: WebUI.takeScreenshot('Data Files/Screenshots/presentation_previewurl.png')

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Assets'))

not_run: WebUI.takeScreenshot('Data Files/Screenshots/presentation_previewurl.png')

//WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Media_favicon'))
not_run: WebUI.uploadFile(findTestObject('Object Repository/Page_Investis Live/a_Media_favicon'), 'C:\\Users\\dinesh.karpenter\\Katalon Studio\\InvestisLive-Webcasting\\Data Files\\test-photo.jpg')

not_run: WebUI.takeScreenshot('Data Files/Screenshots/presentation_previewurl.png')

//'C:\\Users\\dinesh.karpenter\\test-photo.jpg'
not_run: WebUI.uploadFile(findTestObject('Object Repository/Page_Investis Live/a_audioImages'), 'C:\\Users\\dinesh.karpenter\\Katalon Studio\\InvestisLive-Webcasting\\Data Files\\1.jpg')

not_run: WebUI.takeScreenshot('Data Files/Screenshots/presentation_previewurl.png')

not_run: WebUI.uploadFile(findTestObject('Object Repository/Page_Investis Live/a_pdf'), 'C:\\Users\\dinesh.karpenter\\Katalon Studio\\InvestisLive-Webcasting\\Data Files\\SMChart.pdf')

not_run: WebUI.scrollToPosition(9999999, 9999999)

not_run: WebUI.takeScreenshot('Data Files/Screenshots/presentation_previewurl.png')

//WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Streaming'))
//WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Media'))
//WebUI.click(findTestObject('Object Repository/Page_Investis Live/div_Investis Digital     QA_Presentation   _de4038'))
not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Streaming'))

not_run: WebUI.takeScreenshot('Data Files/Screenshots/presentation_previewurl.png')

not_run: WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Investis Live/select_Audio Only        Video Only        _7cac04'), 
    'video', true)

not_run: WebUI.takeScreenshot('Data Files/Screenshots/presentation_previewurl.png')

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Media'))

not_run: WebUI.takeScreenshot('Data Files/Screenshots/presentation_previewurl.png')

not_run: WebUI.uploadFile(findTestObject('Object Repository/Page_Investis Live/a_audioMediaItem'), 'C:\\Users\\dinesh.karpenter\\Katalon Studio\\InvestisLive-Webcasting\\Data Files\\audio.mp3')

not_run: WebUI.waitForElementVisible(findTestObject('Object Repository/Page_Investis Live/a_audiowait'), 30)

not_run: WebUI.takeScreenshot('Data Files/Screenshots/presentation_previewurl.png')

not_run: WebUI.uploadFile(findTestObject('Object Repository/Page_Investis Live/a_videoMediaItem'), 'C:\\Users\\dinesh.karpenter\\Katalon Studio\\InvestisLive-Webcasting\\Data Files\\video.mp4')

not_run: WebUI.scrollToPosition(9999999, 9999999)

not_run: WebUI.takeScreenshot('Data Files/Screenshots/presentation_previewurl.png')

not_run: WebUI.waitForElementVisible(findTestObject('Object Repository/Page_Investis Live/a_videowait'), 1000)

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Start'))

not_run: WebUI.waitForElementVisible(findTestObject('Object Repository/Page_Investis Live/mediaSuccess'), 500)

//The media processing has encountered error. Please Try again. No Video Uploaded
'Verify message after logging in'
not_run: if (WebUI.verifyTextPresent('The media has been processed Successfully.', false)) {
    WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Audience'))
} else {
    WebUI.refresh()

    WebUI.uploadFile(findTestObject('Object Repository/Page_Investis Live/a_audioMediaItem'), 'C:\\Users\\dinesh.karpenter\\Katalon Studio\\InvestisLive-Webcasting\\Data Files\\audio.mp3')

    WebUI.uploadFile(findTestObject('Object Repository/Page_Investis Live/a_videoMediaItem'), 'C:\\Users\\dinesh.karpenter\\Katalon Studio\\InvestisLive-Webcasting\\Data Files\\video.mp4')

    WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Start'))
}

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Investis Digital'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/span_disclaimer'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_disclaimer_span7 viewer-url'))

not_run: WebUI.doubleClick(findTestObject('Object Repository/Page_Investis Live/input_disclaimer_span7 viewer-url'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_disclaimer_span7 viewer-url'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_disclaimer_span7 viewer-url'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_disclaimer_span7 viewer-url'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_disclaimer_span7 viewer-url'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_disclaimer_span7 viewer-url'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Preview URLs'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Investis Digital_1'))

not_run: WebUI.callTestCase(findTestCase('Create_Client'), [:], FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_QA_Presentation'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Create a new presentation'))

not_run: WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_Name_presentationname'), 'QA_CPresentationo1')

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_Password_btn btn-primary'))

not_run: WebUI.openBrowser('')

not_run: WebUI.navigateToUrl('https://staging.investis-live.com/')

not_run: WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_E-mail_loginemail'), 'webcasting@investis.com')

not_run: WebUI.setEncryptedText(findTestObject('Object Repository/Page_Investis Live/input_Password_loginpassword'), 'jNwsIZtkazw/FxUYNNE+Kw==')

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_Password_btn btn-primary'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_Password_btn btn-primary'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Q'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_QA_Presentation_1'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Calendar'))

not_run: WebUI.closeBrowser()

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Create a new client'))

not_run: WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_Name_clientname'), 'QA_cliento3')

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_Password_btn btn-primary'))

not_run: WebUI.callTestCase(findTestCase('Login and Forgot Password'), [:], FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Create a new client'))

not_run: WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_Name_clientname'), 'QA_cliento3')

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_Password_btn btn-primary'))

not_run: WebUI.openBrowser('')

not_run: WebUI.navigateToUrl('https://staging.investis-live.com/')

not_run: WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_E-mail_loginemail'), 'webcasting@investis.com')

not_run: WebUI.setEncryptedText(findTestObject('Object Repository/Page_Investis Live/input_Password_loginpassword'), 'jNwsIZtkazw/FxUYNNE+Kw==')

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_Password_btn btn-primary'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_Select Region_searchText'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Go'))

not_run: WebUI.takeScreenshot('Data Files/Screenshots/clientsrch_blank.png')

not_run: WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_Select Region_searchText'), 'invalid')

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Go'))

not_run: WebUI.takeScreenshot('Data Files/Screenshots/clientsrch_invalid.png')

not_run: WebUI.doubleClick(findTestObject('Object Repository/Page_Investis Live/input_Select Region_searchText'))

not_run: WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_Select Region_searchText'), 'q')

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Go'))

not_run: WebUI.takeScreenshot('Data Files/Screenshots/clientsrch_valid.png')

not_run: WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Investis Live/select_All            UK            Europe(_1284b1'), 
    'UK', true)

not_run: WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_Select Region_searchText'), 'qA')

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Go'))

not_run: WebUI.takeScreenshot('Data Files/Screenshots/clientsrch_regionwse.png')

not_run: WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Investis Live/select_All            UK            Europe(_1284b1'), 
    'USA', true)

not_run: WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_Select Region_searchText'), 'TS')

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Go'))

not_run: WebUI.takeScreenshot('Data Files/Screenshots/clientsrch_blank.png')

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Reset'))

not_run: WebUI.takeScreenshot('Data Files/Screenshots/clientsrch_reset.png')

not_run: WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Investis Live/select_All            UK            Europe(_1284b1'), 
    'All', true)

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Reset'))

not_run: WebUI.takeScreenshot('Data Files/Screenshots/clientsrch_reset1.png')

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Create a new client'))

not_run: WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_Name_clientname'), 'QACLIENT1')

not_run: WebUI.takeScreenshot('Data Files/Screenshots/clients_add.png')

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_Password_btn btn-primary'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Investis Digital'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Investis Digital_1'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Calendar'))

not_run: WebUI.closeBrowser()

not_run: WebUI.navigateToUrl('https://staging.investis-live.com/investis/clients/qaclient1')

not_run: WebUI.navigateToUrl('https://staging.investis-live.com/investis/clients/qaclient1/edit')

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Delete_1_2_3'))

not_run: WebUI.acceptAlert()

not_run: WebUI.openBrowser('')

not_run: WebUI.navigateToUrl('https://staging.investis-live.com/')

not_run: WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_E-mail_loginemail'), 'webcasting@investis.com')

not_run: WebUI.setEncryptedText(findTestObject('Object Repository/Page_Investis Live/input_Password_loginpassword'), 'jNwsIZtkazw/FxUYNNE+Kw==')

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_Password_btn btn-primary'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Q'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_QA-Client'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Undo Delete'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Investis Digital_1'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Q'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_QA-Client'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_HLS'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Client settings'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Cancel'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_HLS'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Convert'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Fetch'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Convert'))

not_run: WebUI.doubleClick(findTestObject('Object Repository/Page_Investis Live/button_Convert'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Convert'))

not_run: WebUI.closeBrowser()

not_run: WebUI.callTestCase(findTestCase('Login and Forgot Password'), [:], FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Create a new client'))

not_run: WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_Name_clientname'), 'QA_cliento3')

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_Password_btn btn-primary'))

not_run: WebUI.callTestCase(findTestCase('Login and Forgot Password'), [:], FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Create a new client'))

not_run: WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_Name_clientname'), 'QA_cliento3')

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_Password_btn btn-primary'))

not_run: WebUI.openBrowser('')

not_run: WebUI.navigateToUrl('https://staging.investis-live.com/')

not_run: WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_E-mail_loginemail'), 'webcasting@investis.com')

not_run: WebUI.setEncryptedText(findTestObject('Object Repository/Page_Investis Live/input_Password_loginpassword'), 'jNwsIZtkazw/FxUYNNE+Kw==')

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_Password_btn btn-primary'))

//username = findTestData('null').getValue('User_Roles/Username, 1')
not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_forgotlink'))

not_run: WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_E-mail_loginemail'), 'webcast@investis.com')

not_run: WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_E-mail_loginemail'), 'webcast@investis.com')

not_run: html((((((((body / (div[2])) / (div[3])) / div) / form) / fieldset) / (div[4])) / div) / a)

'Negative case'
not_run: WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_E-mail_loginemail'), 'webcast@investis.com')

//'webcasting1@investis.com'
not_run: WebUI.setEncryptedText(findTestObject('Object Repository/Page_Investis Live/input_Password_loginpassword'), 'jNwsIZtkazw/FxUYNNE+Kw==')

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_Password_btn btn-primary'))

not_run: WebUI.takeScreenshot('Data Files/Screenshots/Invalid_login.png')

'Blank Case'
not_run: WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_E-mail_loginemail'), '')

not_run: WebUI.setEncryptedText(findTestObject('Object Repository/Page_Investis Live/input_Password_loginpassword'), '')

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_Password_btn btn-primary'))

not_run: WebUI.takeScreenshot('Data Files/Screenshots/Blank_login.png')

'Positive Case'
not_run: WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_E-mail_loginemail'), useremail)

not_run: WebUI.setEncryptedText(findTestObject('Object Repository/Page_Investis Live/input_Password_loginpassword'), password)

//'jNwsIZtkazw/FxUYNNE+Kw=='
not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_Password_btn btn-primary'))

not_run: WebUI.takeScreenshot('Data Files/Screenshots/Valid_login.png')

not_run: WebUI.openBrowser('')

not_run: WebUI.navigateToUrl('https://staging.investis-live.com/')

not_run: WebUI.closeBrowser()

not_run: WebUI.acceptAlert()

not_run: WebUI.click(findTestObject('Page_Investis Live/i_dineshkarpenterinvestisdigitalcom_icon-remove'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Delete'))

not_run: WebUI.callTestCase(findTestCase('Login and Forgot Password'), [:], FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.callTestCase(findTestCase('Login and Forgot Password'), [:], FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Create a new client'))

not_run: WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_Name_clientname'), 'QA_cliento3')

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_Password_btn btn-primary'))

not_run: WebUI.callTestCase(findTestCase('Login and Forgot Password'), [:], FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Create a new client'))

not_run: WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_Name_clientname'), 'QA_cliento3')

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_Password_btn btn-primary'))

not_run: WebUI.openBrowser('')

not_run: WebUI.navigateToUrl('https://staging.investis-live.com/')

not_run: WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_E-mail_loginemail'), 'webcasting@investis.com')

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/fieldset_Organisation loginE-mailPassword F_0b279a'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/fieldset_Organisation loginE-mailPassword F_0b279a'))

not_run: WebUI.setEncryptedText(findTestObject('Object Repository/Page_Investis Live/input_Password_loginpassword'), 'jNwsIZtkazw/FxUYNNE+Kw==')

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_Password_btn btn-primary'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Create a new client'))

not_run: WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_Name_clientname'), 'QA_cliento3')

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_Password_btn btn-primary'))

not_run: WebUI.callTestCase(findTestCase('Login and Forgot Password'), [:], FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Create a new client'))

not_run: WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_Name_clientname'), 'QA_cliento3')

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_Password_btn btn-primary'))

not_run: WebUI.openBrowser('')

not_run: WebUI.navigateToUrl('https://staging.investis-live.com/')

not_run: WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_E-mail_loginemail'), 'webcasting@investis.com')

not_run: WebUI.setEncryptedText(findTestObject('Object Repository/Page_Investis Live/input_Password_loginpassword'), 'jNwsIZtkazw/FxUYNNE+Kw==')

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_Password_btn btn-primary'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Delete'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Delete_1'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Delete_1'))

//WebUI.takeScreenshot('Data Files/Screenshots/Valid_login.png')
not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/legend_Add a new team member'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Delete_1_2'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Delete'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Delete_1_2_3'))

not_run: WebUI.closeBrowser()

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Change Year'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/h1_System Analytics'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Change Year'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Change Year'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Change Year'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/ul_Devices'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Change Year'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Change Year'))

not_run: WebUI.closeBrowser()

not_run: WebUI.callTestCase(findTestCase('Login and Forgot Password'), [:], FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Create a new client'))

not_run: WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_Name_clientname'), 'QA_cliento3')

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_Password_btn btn-primary'))

not_run: WebUI.callTestCase(findTestCase('Login and Forgot Password'), [:], FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Create a new client'))

not_run: WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_Name_clientname'), 'QA_cliento3')

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_Password_btn btn-primary'))

not_run: WebUI.openBrowser('')

not_run: WebUI.navigateToUrl('https://staging.investis-live.com/')

not_run: WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_E-mail_loginemail'), 'webcasting@investis.com')

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/fieldset_Organisation loginE-mailPassword F_0b279a'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/fieldset_Organisation loginE-mailPassword F_0b279a'))

not_run: WebUI.setEncryptedText(findTestObject('Object Repository/Page_Investis Live/input_Password_loginpassword'), 'jNwsIZtkazw/FxUYNNE+Kw==')

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_Password_btn btn-primary'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Change Year'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Change Year'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/div_Calendar  Clients Analytics  System  Re_21b7d1'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Change Year'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Change Year'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Change Year'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Change Year'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Change Year'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Change Year'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Change Year'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Change Year'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Change Year'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Change Year'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/div_System Analytics  Devices              _71c62d'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Change Year'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/h1_System Analytics'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Change Year'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Change Year'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Change Year'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/ul_Devices'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Change Year'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Change Year'))

not_run: WebUI.closeBrowser()

not_run: WebUI.openBrowser('')

not_run: WebUI.navigateToUrl('https://staging.investis-live.com/')

not_run: WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_E-mail_loginemail'), 'webcasting@investis.com')

not_run: WebUI.setEncryptedText(findTestObject('Object Repository/Page_Investis Live/input_Password_loginpassword'), 'jNwsIZtkazw/FxUYNNE+Kw==')

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_Password_btn btn-primary'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Analytics'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_End time_download'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_End time_download'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/div_End time_controls'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_End time_download'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Presenting QA_CPresentationo1  Live Presenter/i_QA_CPresentationo1_icon-unshare-small ico_1d58b5'))

not_run: WebUI.takeScreenshot('Data Files/Screenshots/Valid_login.png')

not_run: WebUI.openBrowser('')

not_run: WebUI.navigateToUrl('https://staging.investis-live.com/')

not_run: WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_E-mail_loginemail'), 'webcasting@investis.com')

not_run: WebUI.setEncryptedText(findTestObject('Object Repository/Page_Investis Live/input_Password_loginpassword'), 'jNwsIZtkazw/FxUYNNE+Kw==')

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/div_Organisation loginE-mailPassword Forgot_101110'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_Password_btn btn-primary'))

not_run: WebUI.closeBrowser()

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Analytics'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_System'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/h1_Mobile'))

not_run: WebUI.takeScreenshot('Data Files/Screenshots/analytics_mobile.png')

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/h1_Desktop'))

not_run: WebUI.takeScreenshot('Data Files/Screenshots/analytics_desktop.png')

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/div_Chrome                                 _53d44e'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/h3_IE'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/h3_Firefox'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/h3_Safari'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/h3_Others'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/h1_Mobile'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/div_Android                                _2e4bcd'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/div_iOS                                    _2703c9'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/div_Windows                                _bc51a9'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/h3_Others'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Change Year'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Change Year'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Change Year'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/div_Calendar  Clients Analytics  System  Re_21b7d1'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/div_System Analytics  Devices              _71c62d'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Analytics'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_System'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/h1_Mobile'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/h1_Desktop'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/div_Chrome                                 _53d44e'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/h3_IE'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/h3_Firefox'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/h3_Safari'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/h3_Others'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/h1_Mobile'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/div_Android                                _2e4bcd'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/div_iOS                                    _2703c9'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/div_Windows                                _bc51a9'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/h3_Others'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Reports'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Investis Digital'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Investis Digital_1'))

not_run: WebUI.takeScreenshot('Data Files/Screenshots/calendar_upcoming.png')

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/h3_Upcoming Presentations'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Previous'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/h3_Previous Presentations'))

not_run: WebUI.doubleClick(findTestObject('Object Repository/Page_Investis Live/h3_Previous Presentations'))

not_run: WebUI.takeScreenshot('Data Files/Screenshots/calendar_previous.png')

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Investis Digital'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Investis Digital_1'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Q'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_QA_cliento1'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Playlist'))

not_run: WebUI.takeScreenshot('Data Files/Screenshots/presentation_playlist.png')

not_run: WebUI.click(findTestObject('Object Repository/Page_Presentation Editor/a_Go make some'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Preview URLs'))

not_run: WebUI.takeScreenshot('Data Files/Screenshots/presentation_preview.png')

not_run: WebUI.switchToWindowIndex(1)

not_run: WebUI.takeScreenshot('Data Files/Screenshots/present_previewwindow.png')

not_run: WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/a_Delivered by Investis Digital'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/a_Help'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/a_FAQ'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/a_Contact Us'))

not_run: WebUI.setText(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/input_Name_contactUsname'), 
    'rewr')

not_run: WebUI.setText(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/input_Email_contactUsemail'), 
    'd@mailinator.com')

not_run: WebUI.setText(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/textarea_Message_contactUsmessage'), 
    'test msg')

not_run: WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/input_Type code shown in image above_btnContactUs'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/a_FAQ'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/a_I cannot view the video'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/a_I cannot hear audio'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/a_The slides are not moving'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/a_I am getting an error Cannot load M3U8 cr_d0d08e'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/a_I cannot register'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/a_I have registered before and I cannot sig_e53755'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/a_Presentation keeps stopping and starting'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/a_I have tried on all browsers but still ca_c6e243'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/a_Is there an alternative way to watch the _f05c3b'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/a_Video says server not found'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/a_There is an echo on the audio'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/a_System requirements'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/a_Browser compatibility'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/a_Mobile compatibility'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/a_Cookies'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/a_Socket support'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/a_Video player test'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/video_Video player test_jw-video jw-reset'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/video_Video player test_jw-video jw-reset'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/i_Slides player test_icon-chevron-down pull-right'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/img'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/a_Bandwidth'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/button_'))

not_run: WebUI.closeBrowser()

not_run: WebUI.openBrowser('')

not_run: WebUI.navigateToUrl('https://staging.investis-live.com/')

not_run: WebUI.rightClick(findTestObject('Object Repository/Page_Investis Live/input_E-mail_loginemail'))

not_run: WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_E-mail_loginemail'), 'webcasting@investis.com')

not_run: WebUI.setEncryptedText(findTestObject('Object Repository/Page_Investis Live/input_Password_loginpassword'), 'jNwsIZtkazw/FxUYNNE+Kw==')

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_Password_btn btn-primary'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Q'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_QA_client001'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Edit'))

not_run: WebUI.switchToWindowTitle('Viewing "qa_presentation001"')

not_run: WebUI.setText(findTestObject('Object Repository/Page_Viewing qa_presentation001/input_Name_usermetadataname'), 
    'qateam')

not_run: WebUI.setText(findTestObject('Object Repository/Page_Viewing qa_presentation001/input_Company_usermetadatacompany'), 
    'qa')

not_run: WebUI.setText(findTestObject('Object Repository/Page_Viewing qa_presentation001/input_Email_useremail'), 'qa@mailinator.com')

not_run: WebUI.click(findTestObject('Object Repository/Page_Viewing qa_presentation001/input_I have read the terms and conditions _365b40'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Viewing qa_presentation001/input_I have read the terms and conditions _180273'))

not_run: WebUI.setText(findTestObject('Object Repository/Page_Viewing qa_presentation001/input_Email_loginemail'), 'dineshkarpenter@mailinator.com')

not_run: WebUI.click(findTestObject('Object Repository/Page_Viewing qa_presentation001/input_I have read the terms and conditions _180273'))

not_run: WebUI.setText(findTestObject('Object Repository/Page_Viewing qa_presentation001/input_Name_usermetadataname'), 
    'qat')

not_run: WebUI.setText(findTestObject('Object Repository/Page_Viewing qa_presentation001/input_Company_usermetadatacompany'), 
    'qa')

not_run: WebUI.setText(findTestObject('Object Repository/Page_Viewing qa_presentation001/input_Email_useremail'), 'qac@mailinator.com')

not_run: WebUI.click(findTestObject('Object Repository/Page_Viewing qa_presentation001/input_I have read the terms and conditions _365b40'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Viewing qa_presentation001/input_I have read the terms and conditions _180273'))

not_run: WebUI.setText(findTestObject('Object Repository/Page_Viewing qa_presentation001/input_Email_loginemail'), 'qac@mailinator.com')

not_run: WebUI.click(findTestObject('Object Repository/Page_Viewing qa_presentation001/input_I have read the terms and conditions _180273'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Viewing qa_presentation001/div_New Viewer                    Name     _e819c5'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Viewing qa_presentation001/button_Accept'))

not_run: WebUI.setText(findTestObject('Object Repository/Page_Viewing qa_presentation001/input_Email_loginemail'), 'qac@mailinator.com')

not_run: WebUI.closeBrowser()

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Analytics'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_System'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/h1_Mobile'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/h1_Desktop'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/div_Chrome                                 _53d44e'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/h3_IE'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/h3_Firefox'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/h3_Safari'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/h3_Others'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/h1_Mobile'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/div_Android                                _2e4bcd'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/div_iOS                                    _2703c9'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/div_Windows                                _bc51a9'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/h3_Others'))

'Verify message after logging in'
if (WebUI.verifyTextPresent('The media has been processed Successfully.', false)) {
    WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Audience'))
} else {
    WebUI.refresh()

    WebUI.uploadFile(findTestObject('Object Repository/Page_Investis Live/a_audioMediaItem'), 'C:\\Users\\dinesh.karpenter\\Katalon Studio\\InvestisLive-Webcasting\\Data Files\\audio.mp3')

    WebUI.uploadFile(findTestObject('Object Repository/Page_Investis Live/a_videoMediaItem'), 'C:\\Users\\dinesh.karpenter\\Katalon Studio\\InvestisLive-Webcasting\\Data Files\\video.mp4')

    WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Start'))
}

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Audience'))

//WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Registration'))
//WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Assets'))
//WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Media'))
//WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Audience'))
WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Update'))

//WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Assets'))
//WebUI.click(findTestObject('Object Repository/Page_Investis Live/i_Audience_icon-user'))
//WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Update'))
//WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Pages'))
//WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Tabs'))
//WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Translations'))
//WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Fill Default'))
//WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Update'))
not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_QA_Presentation_1'))

WebUI.openBrowser('')

WebUI.navigateToUrl('https://staging.investis-live.com/')

WebUI.click(findTestObject('Object Repository/Page_Investis Live/div_Organisation loginE-mailPassword Forgot_101110'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/label_Password'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/div_Organisation loginE-mailPassword Forgot_101110'))

WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_E-mail_loginemail'), 'webcasting@investis.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Investis Live/input_Password_loginpassword'), 'jNwsIZtkazw/FxUYNNE+Kw==')

WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_Password_btn btn-primary'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Q'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_QACLIENT'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Present'))

WebUI.click(findTestObject('Object Repository/Page_Presenting QA-Presentation  Live Presenter/span_Pre-live'))

WebUI.click(findTestObject('Object Repository/Page_Presenting QA-Presentation  Live Presenter/a_Live         Visible to viewers'))

WebUI.click(findTestObject('Object Repository/Page_Presenting QA-Presentation  Live Presenter/button_Activate'))

WebUI.click(findTestObject('Object Repository/Page_Presenting QA-Presentation  Live Presenter/button_Its OK Im testing'))

WebUI.click(findTestObject('Object Repository/Page_Presenting QA-Presentation  Live Presenter/a_Live now  Live'))

WebUI.click(findTestObject('Object Repository/Page_Presenting QA-Presentation  Live Presenter/a_Post-live         Visible to viewers'))

WebUI.click(findTestObject('Object Repository/Page_Presenting QA-Presentation  Live Presenter/button_Activate'))

WebUI.click(findTestObject('Object Repository/Page_Presenting QA-Presentation  Live Presenter/span_Post-live'))

WebUI.click(findTestObject('Object Repository/Page_Presenting QA-Presentation  Live Presenter/a_On-demand       Visible to viewers'))

WebUI.click(findTestObject('Object Repository/Page_Presenting QA-Presentation  Live Presenter/button_Activate'))

WebUI.click(findTestObject('Object Repository/Page_Presenting QA-Presentation  Live Presenter/a_Live now  On-demand'))

WebUI.click(findTestObject('Object Repository/Page_Presenting QA-Presentation  Live Presenter/a_Pre-live         Visible to viewers'))

WebUI.click(findTestObject('Object Repository/Page_Presenting QA-Presentation  Live Presenter/button_Activate'))

WebUI.click(findTestObject('Object Repository/Page_Presenting QA-Presentation  Live Presenter/div_Slide Delay Control'))

WebUI.setText(findTestObject('Object Repository/Page_Presenting QA-Presentation  Live Presenter/input_Delay RTMP (Seconds)_delaySeconds'), 
    '11')

WebUI.click(findTestObject('Object Repository/Page_Presenting QA-Presentation  Live Presenter/button_Update'))

WebUI.click(findTestObject('Object Repository/Page_Presenting QA-Presentation  Live Presenter/button_Slide Delay Control_icon_minimize'))

WebUI.click(findTestObject('Object Repository/Page_Presenting QA-Presentation  Live Presenter/div_Chat Support'))

WebUI.setText(findTestObject('Object Repository/Page_Presenting QA-Presentation  Live Presenter/textarea_Chat Support_chatboxtextarea'), 
    'hi')

WebUI.click(findTestObject('Object Repository/Page_Presenting QA-Presentation  Live Presenter/button_Send'))

WebUI.click(findTestObject('Object Repository/Page_Presenting QA-Presentation  Live Presenter/i_Chat Support_icon-white controlBtn icon-c_2c7962'))

WebUI.click(findTestObject('Object Repository/Page_Presenting QA-Presentation  Live Presenter/a_Add images'))

WebUI.click(findTestObject('Object Repository/Page_Presenting QA-Presentation  Live Presenter/button_Record'))

WebUI.click(findTestObject('Object Repository/Page_Presenting QA-Presentation  Live Presenter/button_Record'))

WebUI.click(findTestObject('Object Repository/Page_Presenting QA-Presentation  Live Presenter/i_Activate_icon-envelope'))

WebUI.setText(findTestObject('Object Repository/Page_Presenting QA-Presentation  Live Presenter/textarea_Message text_message'), 
    'test broad')

WebUI.click(findTestObject('Object Repository/Page_Presenting QA-Presentation  Live Presenter/button_Send message'))

WebUI.click(findTestObject('Object Repository/Page_Presenting QA-Presentation  Live Presenter/a_Add slides'))

WebUI.click(findTestObject('Object Repository/Page_Presenting QA-Presentation  Live Presenter/i_QA-Presentation_icon-unshare-small icon-l_7fa10c'))

WebUI.closeBrowser()

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_QA_Presentation_1'))

not_run: WebUI.callTestCase(findTestCase('Create_Presentation'), [:], FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Edit'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Assets'))

//WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Media_favicon'))
not_run: WebUI.uploadFile(findTestObject('Object Repository/Page_Investis Live/a_Media_favicon'), 'C:\\Users\\dinesh.karpenter\\Katalon Studio\\InvestisLive-Webcasting\\Data Files\\test-photo.jpg')

//'C:\\Users\\dinesh.karpenter\\test-photo.jpg'
not_run: WebUI.uploadFile(findTestObject('Object Repository/Page_Investis Live/a_audioImages'), 'C:\\Users\\dinesh.karpenter\\Katalon Studio\\InvestisLive-Webcasting\\Data Files\\1.jpg')

not_run: WebUI.uploadFile(findTestObject('Object Repository/Page_Investis Live/a_pdf'), 'C:\\Users\\dinesh.karpenter\\Katalon Studio\\InvestisLive-Webcasting\\Data Files\\SMChart.pdf')

//WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Streaming'))
//WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Media'))
//WebUI.click(findTestObject('Object Repository/Page_Investis Live/div_Investis Digital     QA_Presentation   _de4038'))
not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Streaming'))

not_run: WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Investis Live/select_Audio Only        Video Only        _7cac04'), 
    'video', true)

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Media'))

not_run: WebUI.uploadFile(findTestObject('Object Repository/Page_Investis Live/a_audioMediaItem'), 'C:\\Users\\dinesh.karpenter\\Katalon Studio\\InvestisLive-Webcasting\\Data Files\\audio.mp3')

not_run: WebUI.uploadFile(findTestObject('Object Repository/Page_Investis Live/a_videoMediaItem'), 'C:\\Users\\dinesh.karpenter\\Katalon Studio\\InvestisLive-Webcasting\\Data Files\\video.mp4')

not_run: WebUI.waitForElementVisible(findTestObject('Object Repository/Page_Investis Live/mediaSuccess'), 30)

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Start'))

not_run: WebUI.waitForElementVisible(findTestObject('Object Repository/Page_Investis Live/mediaSuccess'), 30)


import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Analytics'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_System'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/h1_Mobile'))

Thread.sleep(GlobalVariable.delaytimeafterstep)

WebUI.takeScreenshot('Data Files/Screenshots/analytics_mobile.png')

WebUI.click(findTestObject('Object Repository/Page_Investis Live/h1_Desktop'))

Thread.sleep(GlobalVariable.delaytimeafterstep)

WebUI.takeScreenshot('Data Files/Screenshots/analytics_desktop.png')

WebUI.click(findTestObject('Object Repository/Page_Investis Live/div_Chrome                                 _53d44e'))

Thread.sleep(GlobalVariable.delaytimeafterstep)

WebUI.click(findTestObject('Object Repository/Page_Investis Live/h3_IE'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/h3_Firefox'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/h3_Safari'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/h3_Others'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/h1_Mobile'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/div_Android                                _2e4bcd'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/div_iOS                                    _2703c9'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/div_Windows                                _bc51a9'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/h3_Others'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Change Year'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Change Year'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Change Year'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/div_Calendar  Clients Analytics  System  Re_21b7d1'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/div_System Analytics  Devices              _71c62d'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Analytics'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Reports'))

WebUI.click(findTestObject('Object Repository/Page_Viewing QA_CPresentationo1 On-demand/button_'))

Thread.sleep(GlobalVariable.delaytimeafterstep)

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Calendar'))

WebUI.takeScreenshot('Data Files/Screenshots/analytics_reportdown.png')


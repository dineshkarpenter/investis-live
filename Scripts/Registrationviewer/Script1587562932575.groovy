import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.navigateToUrl('https://staging.investis-live.com/investis/clients/sunny')

attribute = WebUI.getAttribute(findTestObject('Object Repository/Page_Investis Live/a_GetLiveURL'), 'value')

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Investis Digital'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Logout'))

WebUI.navigateToUrl(attribute)

/*WebUI.navigateToUrl('https://staging.investis-live.com/investis/clients/qaclient10')

attribute = WebUI.getAttribute(findTestObject('Object Repository/Page_Investis Live/a_GetLiveURL'), 'value')

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Investis Digital'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Logout'))

WebUI.navigateToUrl(attribute)

WebUI.openBrowser('')
*/
//WebUI.navigateToUrl('https://staging.investis-live.com/qaclient10/5e9daa5c70167b1000038cc5/qacpresentationo1')
WebUI.setText(findTestObject('Page_Viewing QA_CPresentationo1/input_Name_usermetadataname'), 'QAname')

WebUI.setText(findTestObject('Page_Viewing QA_CPresentationo1/input_Company_usermetadatacompany'), 'QAcomp')

WebUI.setText(findTestObject('Page_Viewing QA_CPresentationo1/input_Email_useremail'), 'dine@mailinator.com')

WebUI.click(findTestObject('Page_Viewing QA_CPresentationo1/input_I have read the terms and conditions _365b40'))

WebUI.click(findTestObject('Page_Viewing QA_CPresentationo1/input_I have read the terms and conditions _180273'))

WebUI.navigateToUrl('https://mailinator.com')

WebUI.setText(findTestObject('Page_Mailinator/input_Mailinator_addOverlay'), 'dine')

WebUI.click(findTestObject('Page_Mailinator/button_GO'))

/*WebUI.click(findTestObject('Page_Mailinator  Main Site/td_alertingserviceinvestisdigitalcom'))

WebUI.switchToWindowTitle('Mailinator | Main Site')
*/
WebUI.click(findTestObject('Page_Mailinator  Main Site/iframe_EMAILclick'))

//attribute = WebUI.getAttribute(findTestObject('Object Repository/Page_Investis Live/a_GetConfirmURL'), 'href')
//WebUI.openBrowser(attribute)
//WebUI.sendKeys(findTestObject('txt_Comment'), Keys.chord(Keys.CONTROL, 't'))
not_run: println(attribute)

not_run: WebUI.navigateToUrl(attribute)

WebUI.switchToFrame(findTestObject('Page_Investis Live/a_emailiframe'), 1)

//driver.getPageSource()
//println('source' + driver.getPageSource())
WebUI.click(findTestObject('Page_Mailinator  Main Site/iframe_EMAILconfirm'))

WebUI.switchToWindowIndex(1)


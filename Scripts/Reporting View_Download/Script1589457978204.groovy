import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

not_run: WebUI.callTestCase(findTestCase('Create_Client'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.navigateToUrl(("$GlobalVariable.StagingURL" + 'investis/clients/') + "$GlobalVariable.client2")

WebUI.click(findTestObject('Page_Investis Live/a_Report_present'))

WebUI.takeScreenshot('Data Files/Screenshots/report_present.png')

WebUI.back(FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Page_Investis Live/a_ reportpage'))

WebUI.takeScreenshot('Data Files/Screenshots/report_page.png')

WebUI.click(findTestObject('Page_Investis Live/a_ Generate_latestreport'))

WebUI.takeScreenshot('Data Files/Screenshots/report_generate.png')

Thread.sleep(GlobalVariable.delaytimeafterstep)

WebUI.refresh(FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Page_Investis Live/a_ Reportview'))

WebUI.switchToWindowIndex(1)

WebUI.closeWindowIndex(1)

WebUI.switchToWindowIndex(0)

WebUI.click(findTestObject('Page_Investis Live/a_ ReportDownload'))

WebUI.back(FailureHandling.STOP_ON_FAILURE)

WebUI.back(FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Page_Investis Live/a_Report_feedback'))

WebUI.takeScreenshot('Data Files/Screenshots/report_feedback.png')

WebUI.takeScreenshot('Data Files/Screenshots/report_downloadpresent.png')

WebUI.back()


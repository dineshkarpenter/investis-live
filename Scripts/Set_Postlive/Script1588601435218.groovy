import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Page_Investis Live/a_Presentclick'))

WebUI.click(findTestObject('Page_Investis Live/a_present_dropdown'))

WebUI.click(findTestObject('Page_Investis Live/a_present_postlive'))

WebUI.click(findTestObject('Page_Investis Live/a_present_postlive _activate'))

Thread.sleep(GlobalVariable.shortdelay)

WebUI.refresh()

WebUI.back()

not_run: WebUI.closeBrowser()

attribute = WebUI.getAttribute(findTestObject('Object Repository/Page_Investis Live/a_GetLiveURL'), 'value')

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Investis Digital'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Logout'))

WebUI.navigateToUrl(attribute)


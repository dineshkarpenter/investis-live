import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.navigateToUrl(GlobalVariable.StagingURL)

WebUI.maximizeWindow(FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Investis Video Player/a_Closed Captions Multilingual'))

WebUI.switchToWindowTitle('Multilingual Closed Captions')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Multilingual Closed Captions/button'))

WebUI.scrollToPosition(999999, 999999)

Thread.sleep(GlobalVariable.delaytimeafterstep)

WebUI.takeScreenshot('Data Files/Screenshots/Video-Players-Screenshots/closedcaption-multi.png')

//WebUI.mouseOver(findTestObject('Video-engine/Page_Multilingual Closed Captions/hoveronplayer'))
//WebUI.click(findTestObject('Video-engine/Page_Multilingual Closed Captions/pause'))
//WebUI.click(findTestObject('Video-engine/Page_Multilingual Closed Captions/cc_hover'))
//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Multilingual Closed Captions/button_1'))
WebUI.sendKeys(findTestObject('Object Repository/Video-engine/Page_Multilingual Closed Captions/cc_hover'), Keys.chord(Keys.TAB))

WebUI.sendKeys(findTestObject('Object Repository/Video-engine/Page_Multilingual Closed Captions/cc_hover'), Keys.chord(Keys.TAB))

/*WebUI.sendKeys(findTestObject('Object Repository/Video-engine/Page_Multilingual Closed Captions/cc_hover'), Keys.chord(Keys.TAB))
WebUI.sendKeys(findTestObject('Object Repository/Video-engine/Page_Multilingual Closed Captions/cc_hover'), Keys.chord(Keys.TAB))
WebUI.sendKeys(findTestObject('Object Repository/Video-engine/Page_Multilingual Closed Captions/cc_hover'), Keys.chord(Keys.TAB))
WebUI.sendKeys(findTestObject('Object Repository/Video-engine/Page_Multilingual Closed Captions/cc_hover'), Keys.chord(Keys.TAB))
WebUI.sendKeys(findTestObject('Object Repository/Video-engine/Page_Multilingual Closed Captions/cc_hover'), Keys.chord(Keys.TAB))
*/
//WebUI.focus(findTestObject('Object Repository/Video-engine/Page_Multilingual Closed Captions/cc_hover'))
WebUI.click(findTestObject('Object Repository/Video-engine/Page_Multilingual Closed Captions/label_Arabic'))

WebUI.delay(2)

/*WebUI.focus(findTestObject('Object Repository/Video player name/Closed Captions/CC button'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Video player name/Closed Captions/Chinese Closed captions'))

WebUI.delay(2)*/
WebUI.closeWindowTitle('Multilingual Closed Captions')

WebUI.switchToWindowTitle('Investis Video Player')


import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://videoengine-staging.investis-live.com/demo/demo.html')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Investis Video Player/a_Self-hosted Video (MP4)'))

WebUI.switchToWindowTitle('Vanilla Player (Self-hosted MP4)')

//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (Self-hosted MP4)/div_Video Player_mejs__overlay-button'))
//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (Self-hosted MP4)/button'))
WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (Self-hosted MP4)/button_1'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (Self-hosted MP4)/button_1_2'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (Self-hosted MP4)/button_1_2_3'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (Self-hosted MP4)/button_1_2_3_4'))

WebUI.switchToWindowTitle('Investis Video Player')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Investis Video Player/a_Youtube'))

WebUI.switchToWindowTitle('Vanilla Player (YouTube)')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (YouTube)/button'))

//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (YouTube)/button'))
//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (YouTube)/button'))
WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (YouTube)/span_000123'))

//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (YouTube)/button'))
WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (YouTube)/button_1'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (YouTube)/button_1_2'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (YouTube)/button_1_2_3'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (YouTube)/div_Google Privacy Policy_mejs__overlay mej_fc2e65'))

WebUI.switchToWindowTitle('Investis Video Player')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Investis Video Player/a_Brightcove'))

WebUI.switchToWindowTitle('Brightcove (Initialization script)')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Brightcove (Initialization script)/div_Video Player_mejs__overlay-button'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Brightcove (Initialization script)/button'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Brightcove (Initialization script)/div_000100000137Use UpDown Arrow keys to in_88f977'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Brightcove (Initialization script)/button_1'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Brightcove (Initialization script)/button_1_2'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Brightcove (Initialization script)/button_1_2_3'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Brightcove (Initialization script)/button_1_2_3_4'))

WebUI.switchToWindowTitle('Investis Video Player')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Investis Video Player/a_Closed Captions Multilingual'))

WebUI.switchToWindowTitle('Multilingual Closed Captions')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Multilingual Closed Captions/button'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Multilingual Closed Captions/button_1'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Multilingual Closed Captions/label_Arabic'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Multilingual Closed Captions/button_1_2'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Multilingual Closed Captions/div_           000006000202000204NoneEnglis_1b69a5'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Multilingual Closed Captions/button_1_2_3'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Multilingual Closed Captions/button_1_2_3_4'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Multilingual Closed Captions/button_1_2_3_4_5'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Multilingual Closed Captions/button'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Multilingual Closed Captions/button_1'))

WebUI.switchToWindowTitle('Investis Video Player')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Investis Video Player/a_HLS Adaptive'))

WebUI.switchToWindowTitle('Vanilla Player (HLS Adaptive)')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (HLS Adaptive)/button'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (HLS Adaptive)/button_1'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (HLS Adaptive)/button_1_2'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (HLS Adaptive)/button_1_2_3'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (HLS Adaptive)/button_1_2_3_4'))

WebUI.switchToWindowTitle('Investis Video Player')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Investis Video Player/a_HLS Live Adaptive'))

WebUI.switchToWindowTitle('HLS Live Adaptive')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_HLS Live Adaptive/div_Network errorDownload File httpsassets-_5d7b9a'))

WebUI.doubleClick(findTestObject('Object Repository/Video-engine/Page_HLS Live Adaptive/div_Network errorDownload File httpsassets-_5d7b9a'))

WebUI.switchToWindowTitle('Investis Video Player')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Investis Video Player/a_HLS Live'))

WebUI.switchToWindowTitle('HLS Live')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_HLS Live/button'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_HLS Live/div_Network errorDownload File httpswowzaec_f1619b'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_HLS Live/button_1'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_HLS Live/button_1_2'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_HLS Live/button_1_2_3'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_HLS Live/button'))

WebUI.switchToWindowTitle('Investis Video Player')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Investis Video Player/a_Accessibility'))

WebUI.switchToWindowTitle('Accessibility')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Accessibility/button'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Accessibility/button_1'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Accessibility/button_1_2'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Accessibility/button_1_2_3'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Accessibility/button_1_2_3_4'))

WebUI.switchToWindowTitle('Investis Video Player')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Investis Video Player/a_Javascript Disabled'))

WebUI.switchToWindowTitle('Javascript Disabled')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Javascript Disabled/button'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Javascript Disabled/button_1'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Javascript Disabled/button_1_2'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Javascript Disabled/button_1_2_3'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Javascript Disabled/button_1_2_3_4'))

WebUI.switchToWindowTitle('Investis Video Player')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Investis Video Player/a_Social Share'))

WebUI.switchToWindowTitle('Social Share')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Social Share/button'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Social Share/button_1'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Social Share/button_Share'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Social Share/div_0000010000000030Share                  _197a22'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Social Share/button_Share'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Social Share/div_0000010000000030Share                  _197a22'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Social Share/button_Share'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Social Share/div_0000010000000030Share                  _197a22'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Social Share/button_Share'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Social Share/div_0000010000000030Share                  _197a22'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Social Share/button_1_2'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Social Share/button_1_2_3'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Social Share/button'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Social Share/button_1_2_3_4'))

WebUI.switchToWindowTitle('Investis Video Player')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Investis Video Player/a_Custom Skin (Theme)'))

WebUI.switchToWindowTitle('Custom Skin')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/button'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/button'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/use'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/svg'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/div_mep_1 mejs__playpause-buttonmejs__pause_93d632'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/use_1'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/div_mep_1 mejs__playpause-buttonmejs__pause_93d632_1'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/use_1'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/div_mep_1 mejs__playpause-buttonmejs__pause_93d632_1_2'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/use_1'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/div_mep_1 mejs__playpause-buttonmejs__pause_93d632_1_2_3'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/svg_Chinese_unmute-icon'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/use_1_2'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/button'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/button_1'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/div_000000360137Share                      _b5a68b'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/label_German'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/label_Arabic'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/button_1_2'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/button'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/label_Chinese'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/button_1'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/div_000002000010000030Share                _979ef9'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/button_1'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/div_000002000010000030Share                _979ef9'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/label_German'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/button_1_2_3'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/button_1_2_3_4'))

WebUI.switchToWindowTitle('Investis Video Player')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Investis Video Player/a_Vimeo'))

WebUI.switchToWindowTitle('Vanilla Player (Vimeo)')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (Vimeo)/button'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (Vimeo)/button'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (Vimeo)/button_1'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (Vimeo)/button_1_2'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (Vimeo)/button'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (Vimeo)/div_Video Player_mejs__overlay-button'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (Vimeo)/div_Video Player_mejs__overlay-button'))

WebUI.switchToWindowTitle('Investis Video Player')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Investis Video Player/a_Popup Player'))

WebUI.switchToWindowTitle('Vanilla Player (YouTube)')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (YouTube)/button'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (YouTube)/button_1'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (YouTube)/div_Video Player_main-investis-player-you-i_e3d9f4'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (YouTube)/button'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (YouTube)/button_1_2_3'))

WebUI.switchToWindowTitle('Investis Video Player')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Investis Video Player/a_Brightcove_1'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Investis Video Player/a_Youtube_1'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Investis Video Player/a_Background Player'))

WebUI.closeBrowser()

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Investis Video Player/a_Youtube'))

WebUI.switchToWindowTitle('Vanilla Player (YouTube)')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (YouTube)/button'))

//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (YouTube)/button'))
//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (YouTube)/button'))
WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (YouTube)/span_000123'))

//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (YouTube)/button'))
WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (YouTube)/button_1'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (YouTube)/button_1_2'))

Thread.sleep(GlobalVariable.delaytimeafterstep)

//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (YouTube)/button_1_2_3'))
//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (YouTube)/div_Google Privacy Policy_mejs__overlay mej_fc2e65'))
WebUI.closeWindowUrl('https://www.youtube.com/watch?v=YE7VzlLtp-4')

WebUI.closeWindowTitle('Vanilla Player (YouTube)')

WebUI.switchToWindowTitle('Investis Video Player')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Investis Video Player/a_Brightcove'))

WebUI.switchToWindowTitle('Brightcove (Initialization script)')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Brightcove (Initialization script)/div_Video Player_mejs__overlay-button'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Brightcove (Initialization script)/button'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Brightcove (Initialization script)/div_000100000137Use UpDown Arrow keys to in_88f977'))

Thread.sleep(GlobalVariable.delaytimeafterstep)

//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Brightcove (Initialization script)/button_1'))
/*WebUI.click(findTestObject('Object Repository/Video-engine/Page_Brightcove (Initialization script)/button_1_2'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Brightcove (Initialization script)/button_1_2_3'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Brightcove (Initialization script)/button_1_2_3_4'))
*/
WebUI.closeWindowTitle('Brightcove (Initialization script)')

WebUI.switchToWindowTitle('Investis Video Player')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Investis Video Player/a_Closed Captions Multilingual'))

WebUI.switchToWindowTitle('Multilingual Closed Captions')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Multilingual Closed Captions/button'))

WebUI.scrollToPosition(999999, 999999)

Thread.sleep(GlobalVariable.delaytimeafterstep)

//WebUI.mouseOver(findTestObject('Video-engine/Page_Multilingual Closed Captions/hoveronplayer'))
//WebUI.click(findTestObject('Video-engine/Page_Multilingual Closed Captions/pause'))
//WebUI.click(findTestObject('Video-engine/Page_Multilingual Closed Captions/cc_hover'))
//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Multilingual Closed Captions/button_1'))
//WebUI.mouseOver(findTestObject('Video-engine/Page_Multilingual Closed Captions/cc_hover'))
//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Multilingual Closed Captions/label_Arabic'))
//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Multilingual Closed Captions/button_1_2'))
//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Multilingual Closed Captions/div_           000006000202000204NoneEnglis_1b69a5'))
//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Multilingual Closed Captions/button_1_2_3'))
//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Multilingual Closed Captions/button_1_2_3_4'))
//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Multilingual Closed Captions/button_1_2_3_4_5'))
//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Multilingual Closed Captions/button'))
//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Multilingual Closed Captions/button_1'))
WebUI.closeWindowTitle('Multilingual Closed Captions')

WebUI.switchToWindowTitle('Investis Video Player')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Investis Video Player/a_HLS Adaptive'))

WebUI.switchToWindowTitle('Vanilla Player (HLS Adaptive)')

Thread.sleep(GlobalVariable.delaytimeafterstep)

//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (HLS Adaptive)/button'))
/*WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (HLS Adaptive)/button_1'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (HLS Adaptive)/button_1_2'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (HLS Adaptive)/button_1_2_3'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (HLS Adaptive)/button_1_2_3_4'))
*/
WebUI.closeWindowTitle('Vanilla Player (HLS Adaptive)')

WebUI.switchToWindowTitle('Investis Video Player')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Investis Video Player/a_HLS Live Adaptive'))

WebUI.switchToWindowTitle('HLS Live Adaptive')

Thread.sleep(GlobalVariable.delaytimeafterstep)

//WebUI.click(findTestObject('Object Repository/Video-engine/Page_HLS Live Adaptive/div_Network errorDownload File httpsassets-_5d7b9a'))
//WebUI.doubleClick(findTestObject('Object Repository/Video-engine/Page_HLS Live Adaptive/div_Network errorDownload File httpsassets-_5d7b9a'))
WebUI.closeWindowTitle('HLS Live Adaptive')

WebUI.switchToWindowTitle('Investis Video Player')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Investis Video Player/a_HLS Live'))

WebUI.switchToWindowTitle('HLS Live')

Thread.sleep(GlobalVariable.delaytimeafterstep)

//WebUI.click(findTestObject('Object Repository/Video-engine/Page_HLS Live/button'))
//WebUI.click(findTestObject('Object Repository/Video-engine/Page_HLS Live/div_Network errorDownload File httpswowzaec_f1619b'))
//WebUI.click(findTestObject('Object Repository/Video-engine/Page_HLS Live/button_1'))
//WebUI.click(findTestObject('Object Repository/Video-engine/Page_HLS Live/button_1_2'))
//WebUI.click(findTestObject('Object Repository/Video-engine/Page_HLS Live/button_1_2_3'))
//WebUI.click(findTestObject('Object Repository/Video-engine/Page_HLS Live/button'))
WebUI.closeWindowTitle('HLS Live')

WebUI.switchToWindowTitle('Investis Video Player')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Investis Video Player/a_Accessibility'))

WebUI.switchToWindowTitle('Accessibility')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Accessibility/button'))

Thread.sleep(GlobalVariable.delaytimeafterstep)

//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Accessibility/button_1'))
/*WebUI.click(findTestObject('Object Repository/Video-engine/Page_Accessibility/button_1_2'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Accessibility/button_1_2_3'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Accessibility/button_1_2_3_4'))
*/
WebUI.closeWindowTitle('Accessibility')

WebUI.switchToWindowTitle('Investis Video Player')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Investis Video Player/a_Javascript Disabled'))

WebUI.switchToWindowTitle('Javascript Disabled')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Javascript Disabled/button'))

Thread.sleep(GlobalVariable.delaytimeafterstep)

//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Javascript Disabled/button_1'))
/*WebUI.click(findTestObject('Object Repository/Video-engine/Page_Javascript Disabled/button_1_2'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Javascript Disabled/button_1_2_3'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Javascript Disabled/button_1_2_3_4'))*/
WebUI.closeWindowTitle('Javascript Disabled')

WebUI.switchToWindowTitle('Investis Video Player')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Investis Video Player/a_Social Share'))

WebUI.switchToWindowTitle('Social Share')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Social Share/button'))

Thread.sleep(GlobalVariable.delaytimeafterstep)

//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Social Share/button_1'))
/*WebUI.click(findTestObject('Object Repository/Video-engine/Page_Social Share/button_Share'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Social Share/div_0000010000000030Share                  _197a22'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Social Share/button_Share'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Social Share/div_0000010000000030Share                  _197a22'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Social Share/button_Share'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Social Share/div_0000010000000030Share                  _197a22'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Social Share/button_Share'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Social Share/div_0000010000000030Share                  _197a22'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Social Share/button_1_2'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Social Share/button_1_2_3'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Social Share/button'))
*/
//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Social Share/button_1_2_3_4'))
WebUI.closeWindowTitle('Social Share')

WebUI.switchToWindowTitle('Investis Video Player')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Investis Video Player/a_Custom Skin (Theme)'))

WebUI.switchToWindowTitle('Custom Skin')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/button'))

Thread.sleep(GlobalVariable.delaytimeafterstep)

//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/button'))
/*WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/use'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/svg'))
*/
WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/div_mep_1 mejs__playpause-buttonmejs__pause_93d632'))

Thread.sleep(GlobalVariable.delaytimeafterstep)

//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/use_1'))
//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/div_mep_1 mejs__playpause-buttonmejs__pause_93d632_1'))
//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/use_1'))
//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/div_mep_1 mejs__playpause-buttonmejs__pause_93d632_1_2'))
//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/use_1'))
//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/div_mep_1 mejs__playpause-buttonmejs__pause_93d632_1_2_3'))
//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/svg_Chinese_unmute-icon'))
//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/use_1_2'))
//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/button'))
WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/button_1'))

Thread.sleep(GlobalVariable.delaytimeafterstep)

/*WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/div_000000360137Share                      _b5a68b'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/label_German'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/label_Arabic'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/button_1_2'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/button'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/label_Chinese'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/button_1'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/div_000002000010000030Share                _979ef9'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/button_1'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/div_000002000010000030Share                _979ef9'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/label_German'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/button_1_2_3'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Custom Skin/button_1_2_3_4'))*/
WebUI.closeWindowTitle('Custom Skin')

WebUI.switchToWindowTitle('Investis Video Player')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Investis Video Player/a_Vimeo'))

WebUI.switchToWindowTitle('Vanilla Player (Vimeo)')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (Vimeo)/button'))

Thread.sleep(GlobalVariable.delaytimeafterstep)

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (Vimeo)/button'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (Vimeo)/button_1'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (Vimeo)/button_1_2'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (Vimeo)/button'))

Thread.sleep(GlobalVariable.delaytimeafterstep)

//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (Vimeo)/div_Video Player_mejs__overlay-button'))
//WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (Vimeo)/div_Video Player_mejs__overlay-button'))
WebUI.closeWindowTitle('Vanilla Player (Vimeo)')

WebUI.switchToWindowTitle('Investis Video Player')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Investis Video Player/a_Popup Player'))

WebUI.switchToWindowTitle('Vanilla Player (YouTube)')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (YouTube)/button'))

Thread.sleep(GlobalVariable.delaytimeafterstep)

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (YouTube)/button_1'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (YouTube)/div_Video Player_main-investis-player-you-i_e3d9f4'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (YouTube)/button'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Vanilla Player (YouTube)/button_1_2_3'))

Thread.sleep(GlobalVariable.delaytimeafterstep)

WebUI.closeWindowTitle('Vanilla Player (YouTube)')

WebUI.switchToWindowTitle('Investis Video Player')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Investis Video Player/a_Brightcove_1'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Investis Video Player/a_Youtube_1'))

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Investis Video Player/a_Background Player'))

WebUI.closeBrowser()


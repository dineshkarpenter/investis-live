import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_Select Region_searchText'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Go'))

Thread.sleep(GlobalVariable.delaytimeafterstep)

WebUI.takeScreenshot('Data Files/Screenshots/clientsrch_blank.png')

WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_Select Region_searchText'), 'invalid')

WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Go'))

Thread.sleep(GlobalVariable.delaytimeafterstep)

WebUI.takeScreenshot('Data Files/Screenshots/clientsrch_invalid.png')

WebUI.doubleClick(findTestObject('Object Repository/Page_Investis Live/input_Select Region_searchText'))

WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_Select Region_searchText'), 'q')

WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Go'))

WebUI.takeScreenshot('Data Files/Screenshots/clientsrch_valid.png')

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Investis Live/select_All            UK            Europe(_1284b1'), 
    'UK', true)

WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_Select Region_searchText'), 'qA')

WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Go'))

WebUI.takeScreenshot('Data Files/Screenshots/clientsrch_regionwse.png')

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Investis Live/select_All            UK            Europe(_1284b1'), 
    'USA', true)

WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_Select Region_searchText'), 'TS')

WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Go'))

Thread.sleep(GlobalVariable.delaytimeafterstep)

WebUI.takeScreenshot('Data Files/Screenshots/clientsrch_blank.png')

WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Reset'))

Thread.sleep(GlobalVariable.delaytimeafterstep)

WebUI.takeScreenshot('Data Files/Screenshots/clientsrch_reset.png')

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Investis Live/select_All            UK            Europe(_1284b1'), 
    'All', true)

WebUI.click(findTestObject('Object Repository/Page_Investis Live/button_Reset'))

WebUI.takeScreenshot('Data Files/Screenshots/clientsrch_reset1.png')

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Create a new client'))

WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_Name_clientname'), GlobalVariable.client1)

WebUI.takeScreenshot('Data Files/Screenshots/clients_add.png')

WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_Password_btn btn-primary'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Create a new client'))

WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_Name_clientname'), GlobalVariable.client2)

WebUI.takeScreenshot('Data Files/Screenshots/clients_add.png')

WebUI.click(findTestObject('Object Repository/Page_Investis Live/input_Password_btn btn-primary'))

Thread.sleep(GlobalVariable.delaytimeafterstep)

//"${GlobalVariable.StagingURL}"+'investis/client'+"${GlobalVariable.useremail2}"
WebUI.navigateToUrl(("$GlobalVariable.StagingURL" + 'investis/clients/') + "$GlobalVariable.client1")

WebUI.navigateToUrl((("$GlobalVariable.StagingURL" + 'investis/clients/') + "$GlobalVariable.client1") + '/edit')

//WebUI.navigateToUrl('https://staging.investis-live.com/investis/clients/qaclient9/edit')
WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_Name_clientname_1'), 'QA-Client_updated')

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Investis Live/select_UK                Europe(Non UK)    _9dd199'), 
    'Europe', true)

WebUI.click(findTestObject('Page_Investis Live/button_Update_client'))

Thread.sleep(GlobalVariable.delaytimeafterstep)

WebUI.takeScreenshot('Data Files/Screenshots/clientsrch_reset1.png')

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Delete_1_2_3'))

not_run:WebUI.acceptAlert(FailureHandling.STOP_ON_FAILURE)

Thread.sleep(GlobalVariable.delaytimeafterstep)


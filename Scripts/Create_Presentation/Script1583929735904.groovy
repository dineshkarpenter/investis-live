import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

not_run: WebUI.callTestCase(findTestCase('Create_Client'), [:], FailureHandling.STOP_ON_FAILURE)

//string URL = "https://staging.investis-live.com"
//WebUI.callTestCase(findTestCase('URL_Settings'), [:], FailureHandling.STOP_ON_FAILURE)
//WebUI.navigateToUrl( stageurl+"/investis/clients/qaclient10")
//"${GlobalVariable.StagingURL}"+'investis/client'+"${GlobalVariable.useremail2}"
WebUI.navigateToUrl(("$GlobalVariable.StagingURL" + 'investis/clients/') + "$GlobalVariable.client2")

//https://staging.investis-live.com/investis/clients/qaclient10
not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Q'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_QA_Presentation'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Create a new presentation'))

WebUI.setText(findTestObject('Object Repository/Page_Investis Live/input_Name_presentationname'), 'QA_CPresentationo1')

WebUI.click(findTestObject('Page_Investis Live/input_presentcreatebutton'))

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Preview URLs'))

Thread.sleep(GlobalVariable.delaytimeafterstep)

WebUI.takeScreenshot('Data Files/Screenshots/presentation_previewurl.png')

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Questions'))

WebUI.takeScreenshot('Data Files/Screenshots/presentation_questions.png')

not_run: WebUI.click(findTestObject('Object Repository/Page_Investis Live/div_  Questions will come in real time when_010760'))

WebUI.back()

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Launch and edit theme'))

Thread.sleep(GlobalVariable.delaytimeafterstep)

WebUI.takeScreenshot('Data Files/Screenshots/present_launchedittheme.png')

WebUI.click(findTestObject('Object Repository/Page_Viewing QA_newpresent1/a_Pre-live'))

WebUI.takeScreenshot('Data Files/Screenshots/present_prelive.png')

WebUI.click(findTestObject('Object Repository/Page_Viewing QA_newpresent1/a_Registration'))

WebUI.click(findTestObject('Object Repository/Page_Viewing QA_newpresent1/a_Registration_1'))

WebUI.takeScreenshot('Data Files/Screenshots/present_registrationpng')

WebUI.click(findTestObject('Object Repository/Page_Viewing QA_newpresent1/a_Disclaimer'))

WebUI.click(findTestObject('Object Repository/Page_Viewing QA_newpresent1/a_Disclaimer_1'))

WebUI.takeScreenshot('Data Files/Screenshots/present_disclaimer.png')

WebUI.click(findTestObject('Object Repository/Page_Viewing QA_newpresent1/a_Live'))

WebUI.click(findTestObject('Object Repository/Page_Viewing QA_newpresent1/a_Live_1'))

WebUI.takeScreenshot('Data Files/Screenshots/present_live.png')

WebUI.click(findTestObject('Object Repository/Page_Viewing QA_newpresent1/a_Post-live'))

WebUI.click(findTestObject('Object Repository/Page_Viewing QA_newpresent1/a_Post-live_1'))

WebUI.takeScreenshot('Data Files/Screenshots/present_postlive.png')

WebUI.click(findTestObject('Object Repository/Page_Viewing QA_newpresent1/a_On-demand'))

WebUI.takeScreenshot('Data Files/Screenshots/present_ondemand.png')

/*WebUI.click(findTestObject('Object Repository/Page_Viewing QA_newpresent1 On-demand/a_Edit theme'))

WebUI.takeScreenshot('Data Files/Screenshots/present_edittheme.png')

WebUI.click(findTestObject('Object Repository/Page_Viewing QA_newpresent1 On-demand/button_Save'))

WebUI.takeScreenshot('Data Files/Screenshots/present_savetheme.png')

WebUI.click(findTestObject('Object Repository/Page_Viewing QA_newpresent1 On-demand/button_'))



not_run: WebUI.closeBrowser()
*/
WebUI.back()

WebUI.click(findTestObject('Page_Investis Live/a_Adminback'))

WebUI.click(findTestObject('Page_Investis Live/a_HLS'))

Thread.sleep(GlobalVariable.delaytimeafterstep)

WebUI.takeScreenshot('Data Files/Screenshots/present_savetheme.png')

WebUI.click(findTestObject('Page_Investis Live/a_HLSfetchnew'))

WebUI.back()

WebUI.click(findTestObject('Object Repository/Page_Investis Live/a_Playlist'))

Thread.sleep(GlobalVariable.delaytimeafterstep)

WebUI.takeScreenshot('Data Files/Screenshots/presentation_playlist.png')

WebUI.click(findTestObject('Object Repository/Page_Presentation Editor/a_Go make some'))

WebUI.back()

WebUI.back()

WebUI.click(findTestObject('Page_Investis Live/a_Presentclick'))

Thread.sleep(GlobalVariable.delaytimeafterstep)

WebUI.takeScreenshot('Data Files/Screenshots/presentation_present.png')

WebUI.back()


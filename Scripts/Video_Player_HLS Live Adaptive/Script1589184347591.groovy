import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.navigateToUrl(GlobalVariable.StagingURL)

WebUI.click(findTestObject('Object Repository/Video-engine/Page_Investis Video Player/a_HLS Live Adaptive'))

WebUI.switchToWindowTitle('HLS Live Adaptive')

Thread.sleep(GlobalVariable.delaytimeafterstep)

WebUI.takeScreenshot('Data Files/Screenshots/Video-Players-Screenshots/hls-live-adaptive.png')

WebUI.click(findTestObject('Object Repository/Video-engine/Page_HLS Live Adaptive/div_Network errorDownload File httpsassets-_5d7b9a'))
//WebUI.doubleClick(findTestObject('Object Repository/Video-engine/Page_HLS Live Adaptive/div_Network errorDownload File httpsassets-_5d7b9a'))
WebUI.closeWindowTitle('HLS Live Adaptive')

WebUI.switchToWindowTitle('Investis Video Player')


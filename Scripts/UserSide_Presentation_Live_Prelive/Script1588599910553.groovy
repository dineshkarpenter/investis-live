import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

not_run: WebUI.closeWindowIndex(0)

not_run: WebUI.switchToWindowIndex(0)

WebUI.setText(findTestObject('Page_Viewing QA_CPresentationo1/input_Name_usermetadataname'), 'QAname')

WebUI.setText(findTestObject('Page_Viewing QA_CPresentationo1/input_Company_usermetadatacompany'), 'QAcomp')

WebUI.setText(findTestObject('Page_Viewing QA_CPresentationo1/input_Email_useremail'), 'dinesh.karpenter@investisdigital.com')

WebUI.setText(findTestObject('Page_Investis Live/a_email_password_signin'), 'wPThvGzJ7ekWKPly70yk9Q==')

WebUI.check(findTestObject('Page_Viewing QA_CPresentationo1/input_I have read the terms and conditions _180273'))

WebUI.submit(findTestObject('Page_Investis Live/a_viewersignbutton'))

WebUI.click(findTestObject('Page_Mailinator  Main Site/a_disclaimer'))

Thread.sleep(GlobalVariable.shortdelay)

WebUI.click(findTestObject('Page_Investis Live/a_Prelive_Videonslides'))

Thread.sleep(GlobalVariable.shortdelay)

WebUI.click(findTestObject('Page_Investis Live/a_Prelive_Audionslides'))

Thread.sleep(GlobalVariable.shortdelay)

WebUI.takeScreenshot('Data Files/Screenshots/Viewer_Prelive.png')

WebUI.back()

WebUI.back()


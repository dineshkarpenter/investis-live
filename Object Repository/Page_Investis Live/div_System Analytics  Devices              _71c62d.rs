<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_System Analytics  Devices              _71c62d</name>
   <tag></tag>
   <elementGuidId>3b0e2538-069e-4eb6-922e-b77e8ca261c3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Logout'])[1]/following::div[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>container</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>

		


		System Analytics



  Devices



    
        
            Devices: Since Jan 01, 2020
            
                
                Change Year
            
            
            
        
        
            
                 
                  Mobile
                  
                    
                    Count : 46 (18.70%)
                  
                    
                
            
            
                 
                  Desktop
                  
                    
                    Count : 200 (81.30%)
                  
                   
                
            
           
        

        
            
                 
                  Android
                   
                    
                    Count : 1 (2.17%)
                  
                
            
            
                 
                  iOS
                  
                    
                    Count : 45 (97.83%)
                  
                
             
            
                 
                  Windows
                  
                    
                    Count : 0 (0.00%)
                  
                
             
            
                 
                  Others
                  
                    
                    Count : 0 (0.00%)
                  
                
            
        
        
            
                
                     
                      Chrome
                      
                        
                        Count : 175 (87.50%)
                      
                    
                
                
                     
                      IE
                      
                        
                        Count : 10 (5.00%)
                      
                    
                 
                
                     
                      Firefox
                      
                        
                        Count : 14 (7.00%)
                      
                    
                
            
            
                 
                     
                      Safari
                      
                        
                        Count : 1 (0.50%)
                      
                    
                
                
                     
                      Others
                      
                        
                        Count : 0 (0.00%)
                      

                
            
        
    


    $(&quot;#mobile&quot;).off().on(&quot;click&quot;, function(){
        $(&quot;#details-mobile&quot;).show();
        $(&quot;#details-desktop&quot;).hide();
    });
    $(&quot;#desktop&quot;).off().on(&quot;click&quot;, function(){
        $(&quot;#details-mobile&quot;).hide();
        $(&quot;#details-desktop&quot;).show();
    });
    $(&quot;#changeYear&quot;).off().on(&quot;click&quot;, function(){
        var year = prompt(&quot;Enter the year: &quot;);
        if(!year) return;
        try{
            year = parseInt(year);
        }catch(e){
            alert(&quot;Please enter valid year above 2015&quot;);
        }
        if(isNaN(year) || year &lt; 2016 || year > (new Date).getFullYear()){
            alert(&quot;Please enter valid year above 2015&quot;);
        }else{
            window.location.href = window.location.origin + window.location.pathname + &quot;?year=&quot;+year; 
        }
    })


	

  	
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-60959306-1', 'auto');
  ga('send', 'pageview');
	




</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;container&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Logout'])[1]/following::div[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Investis Digital'])[2]/following::div[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]</value>
   </webElementXpaths>
</WebElementEntity>

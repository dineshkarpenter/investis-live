<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Investis Digital     QA_Presentation   _de4038</name>
   <tag></tag>
   <elementGuidId>a1396855-0535-40a3-aeaa-bdfb1f5f8a6f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Logout'])[1]/following::div[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>container</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>

		


		
  
    Investis Digital /
  
	
	  QA_Presentation /
	
	
	  QA_CPresentation — Edit
	



	Editing QA_CPresentation
  Preview



   Details
   Assets
   Streaming
   Media
   Audience
   Pages
   Tabs
   Translations
   Polls
  .myInvestis



	

  

  
    Title
    
  

  
          
    Start time
    
      
    
    
    
      Please note: The start time is in GMT. The start time used to organise the presentation. To display start time to your viewers please enter your preferred time details into the page content sections.
    
    
  
  
  
  
  
  
  

  
  
  
      
          
            Enable Email Reminder
          
      
  
   
    Reminder Time

    
      
        
   

  
    Warning: Reminder Time cannot be configured as the webcast is either about to start or start time has already passed.
  

  

  
  
    Total Duration (h)
    
      
      Default : 2 This value will set the calendar event duration. (e.g 1.5 = 1h 30m)Minimum : 0.5 (i.e. 30 minutes) and Maximum : 4 Days (i.e. 99 hours)
      
    
  

  
    Contact Email
    
      
      This email will be displayed to your viewers so they can contact you if they have queries.
    
  

  
    Feedback Email
    
      
      (Optional) These email(s) will be used to send Feedack Mails along with the owners. Make sure the emails are in proper format and separate them by comma &quot;,&quot;
    
  

  
    Logo URL
    
      
      (Optional) This URL will be used for redirection when the user clicks on the Logo. Make sure the url is correct. Eg: http://www.investis-live.com
    
  

  
      
          Error: There is some error so you are not able to submit.
      
    Update
    Cancel
  



 
 
  //Initialization
  var $startTime = $(&quot;#startTime&quot;);
  var $reminderTime = $(&quot;#remDateTime&quot;);
  var $reminderMsg = $(&quot;#reminderMsg&quot;);
  var $reminderMsgText = $(&quot;#reminderMsg .msg&quot;);
  var $reminderTitleText = $(&quot;#reminderMsg .note&quot;);
  var $reminderSentMsg = $(&quot;#reminderSentMsg&quot;);
  var $reminderSentMsgText = $(&quot;#reminderSentMsg .msg&quot;);
  var $reminderCheckbox = $(&quot;#isRemiderEnable&quot;);
  var $timepicker = $(&quot;#remctrl&quot;);
  var $timePickerField = $(&quot;#datetimes&quot;);
  var $reminderStatus = $(&quot;#reminderStatus&quot;);
  var isPreLive = ($(&quot;#presentationStatus&quot;).val() == 'pre' || $(&quot;#presentationStatus&quot;).val() == 'live');
  var dateFormat = &quot;DD/MM/YYYY hh:mm A&quot;; //This format will be used throughout for reminder Time

  //Returns nearest reminder time that can be configured
  var getNearestValidTime = function(){
    var date = new Date();
    return (moment(date).minutes()%5 == 0) ? moment(date).add(5, 'm').set({second:0,millisecond:0}) : moment(date).add((5 - (moment(date).minutes()%5)), 'm').set({second:0,millisecond:0});
  };

  //Checking if reminder time is already configured
  var reminderTime = ($reminderTime.val() != '' ? moment($reminderTime.val()).toDate() : getNearestValidTime());
  $reminderTime.val(moment(reminderTime).valueOf());

  $(document).ready(function(){ 
    
    if($('#guestlistEntry').hasClass('in') &amp;&amp; $reminderCheckbox.val() == &quot;true&quot;)
    {      
      $(&quot;#registraionTypeMsg&quot;).css(&quot;display&quot;,&quot;block&quot;);
      $registraionMsgTitle.text(&quot;Please note:&quot;);
      $registraionMsgText.text(&quot;Email reminders if setup will be sent to only registered users from the guest list.&quot;);
    }

    //Setting up reminder fields(checkbox &amp; timepicker) on page load
    setupReminderFields();

    //Handling Start Time change event
    $(&quot;#ui-date-startTime, #ui-time-startTime&quot;).on(&quot;change&quot;,function(){
      resetReminder();
      setupReminderFields();
    });

    //Handling Registration method/Predefined change events
    $(&quot;input[type=radio][name=presentation[viewerAuthMethod]], input[type=checkbox][name='presentation[registrationRequired]']&quot;).on(&quot;change&quot;,function(){
      setupReminderFields();
    });

    //Hiding/showing timepicker based on Enable email reminder checkbox
    $reminderCheckbox.click(function(){
      if($('#guestlistEntry').hasClass('in'))
      {        
        $(&quot;#registraionTypeMsg&quot;).css(&quot;display&quot;,&quot;block&quot;);
        $registraionMsgTitle.text(&quot;Please note:&quot;);
        $registraionMsgText.text(&quot;Email reminders if setup will be sent to only registered users from the guest list.&quot;);        
      }
      if($(this).prop('checked')){
        setNearestValidTime();
        $(this).val(true);
        $timepicker.show();
      }else{
        $reminderTime.val(moment(reminderTime).valueOf());
        $(this).val(false);
        $timepicker.hide();
        $(&quot;#registraionTypeMsg&quot;).css(&quot;display&quot;,&quot;none&quot;);
      }
      markReminderTimeValid();
    });

    //Checking if reminder time is valid on Submit click
    $(&quot;#edit-form&quot;).on(&quot;submit&quot;,function(e){
      var reminderActive = ($reminderCheckbox.val()==&quot;true&quot; || $reminderCheckbox.val()==true);
      if(!checkReminderTimeValidity()){
        markReminderTimeInvalid();
        alert(&quot;There is some error so you are not able to submit.&quot;);
        return false;
      }else{
        //Validating mandatory fields other than Email Reminder
        if(!validateMandatoryFields()){
          return false;
        }
        if(reminderActive){
          $reminderStatus.val(false);
        }else{
          $reminderTime.val(moment(reminderTime).valueOf());
        }
        if ($('#emailCustomizationErrorId').length === 0 &amp;&amp; $('form#edit-form').valid()) {
          // Don't submit the file input fields (these use a separate endpoint, via the upload plugin
          $(&quot;input[type=file]&quot;, this).each(function () {
            $(this).remove();
          });
          $('#customEmailTemplateForm .requiredField').removeAttr('name');
        }
      }
    });
  });

  /*---------------------------------------------------------------------*/
  //VALIDATING ALL MANDATORY FIELDS OTHER THAN EMAIL REMINDER
  /*---------------------------------------------------------------------*/
  var validateMandatoryFields = function(){
    $('#errTotalDuration').remove();
    //Total Duration under Details Tab
    if($(&quot;#totalDuration&quot;).val().trim() == &quot;&quot; &amp;&amp; $('#field-tabs li.active a').attr('href') != '#details'){
      var errTotalDuration = bootstrapAlertMessage(&quot;error&quot;,&quot;Please provide &lt;b>Total Duration&lt;/b> value under &lt;b>Details&lt;/b> tab.&quot;,&quot;errTotalDuration&quot;,true);
      $('.breadcrumb').before(errTotalDuration);
      $('html, body').scrollTop(0);
      return false;
    }
    return true;
  }

  //Removing validation message once mandatory fields are filled
  $(&quot;#totalDuration&quot;).blur(function(){
    if($(&quot;#totalDuration&quot;).val().trim() != &quot;&quot; &amp;&amp; $('#errTotalDuration').length != 0){
      //Setting delay to preserve tab click. Otherwise tab change doesn't work because it shifts up
      setTimeout(function(){$('#errTotalDuration').remove();},150);
    }
  });

  /*---------------------------------------------------------------------*/
  //MAIN FUNCTION TO SETUP REMINDER FIELDS DEPENDING UPON ALL CONDITIONS
  /*---------------------------------------------------------------------*/
  var setupReminderFields = function(){
    //Registration Required or Not
    var regMethodDisabled = $(&quot;input[type=checkbox][name='presentation[registrationRequired]']:checked&quot;).length == 0;
    //Authentication method is predefined
    var isPredefined = ($(&quot;input[type=radio][name=presentation[viewerAuthMethod]]:checked&quot;).val() == &quot;predefined&quot;);
    //Current Time
    var currentTime = new Date();
    var reminderActive = ($reminderCheckbox.val()==&quot;true&quot; || $reminderCheckbox.val()==true);
    var reminderStatus = ($reminderStatus.val()==&quot;true&quot; || $reminderStatus.val()==true);
    
    if(regMethodDisabled){
      //Disabling Reminder Fields as Registration is not required
      disableReminderFields();
      showReminderNote(messageTypes.warning, reminderMessages.RegRequiredMsg);
    }else if(isPredefined){
      //Disabling Reminder Fields as Registration Method is Predefined
      disableReminderFields();
      showReminderNote(messageTypes.warning, reminderMessages.PredefinedMsg);
    }else if(!isPreLive){
      //Disabling Reminder Fields as Webcast is not in Pre-live state
      disableReminderFields();
      showReminderNote(messageTypes.warning, reminderMessages.PreLiveMessage);
    }else if(moment($startTime.val()).valueOf() &lt;= moment(getNearestValidTime()).add(4, 'm').valueOf()){
      //Disabling Reminder Fields as Presentation Start Time is less than 15 mins from now
      disableReminderFields();
      showReminderNote(messageTypes.warning, reminderMessages.AboutToStart);
    }else{
      //Marking reminder time as valid
      markReminderTimeValid();
      //Enabling reminder fields as it's valid
      reminderActive ? activeAndEnabledReminderFields(): enableReminderFields();
      setupDatetimePicker();
      //Displaying info
      showReminderNote(messageTypes.info, reminderMessages.ReminderInfo);
    }
  };

  /*---------------------------------------------------------------------*/
  //FUNCTION TO SETUP DATERANGEPICKER
  /*---------------------------------------------------------------------*/
  var setupDatetimePicker = function(){
    var currentReminderTime = moment(parseInt($reminderTime.val())).toDate();
    //Initializing daterangepicker
    $timePickerField.daterangepicker(
    {
        autoUpdateInput: true,
        timePicker: true,
        timePicker24Hour:false,
        singleDatePicker: true,   
        showDropdowns: true,  
        timePickerIncrement:5,
        locale: {
          format: 'DD/MM/YYYY hh:mm A'
        },
        startDate: currentReminderTime
    });
    //Handling Apply button click event of daterangepicker
    $timePickerField.on('hide.daterangepicker', function(ev, picker) 
    { 
      if(!checkReminderTimeValidity()){
        markReminderTimeInvalid();
      }else{
        markReminderTimeValid();
        $reminderTime.val(Date.parse(picker.startDate));
        $(this).val(picker.startDate.format(&quot;DD/MM/YYYY hh:mm A&quot;));
      }
    });
  };

  /*---------------------------------------------------------------------*/
  //FUNCTION TO CHECK VALIDITY OF REMINDER TIME
  /*---------------------------------------------------------------------*/
  var checkReminderTimeValidity = function(){
    //Variables required for validation
    var reminderActive = ($reminderCheckbox.val()==&quot;true&quot; || $reminderCheckbox.val()==true);
    //Converting all times to milliseconds for comparison
    var startTime = moment($startTime.val()).valueOf();
    var reminderPickerTime = moment($timePickerField.val(),dateFormat).set({second:0,millisecond:0}).valueOf();
    var currentTime = moment(moment().valueOf()).set({second:0,millisecond:0}).valueOf();
    
    if(reminderActive &amp;&amp; ((startTime &lt;= reminderPickerTime) || (reminderPickerTime &lt; currentTime) || (startTime-reminderPickerTime&lt;300000))){
      return false;
    }else{
      return true;
    }
  };

  /*---------------------------------------------------------------------*/
  //FUNCTION TO SET NEAREST VALID TIME IN REMINDER TIME PICKER
  /*---------------------------------------------------------------------*/
  var setNearestValidTime = function(){
    //Setting nearest valid time in reminder time
    var nearestValidTime = moment(getNearestValidTime());
    $timePickerField.data('daterangepicker').setStartDate(nearestValidTime);
    $timePickerField.data('daterangepicker').setEndDate(nearestValidTime);
    $reminderTime.val(nearestValidTime.valueOf());
  };

  /*---------------------------------------------------------------------*/
  //RESETTING REMINDER TIME FIELDS WHEN START TIME IS CHANGED
  /*---------------------------------------------------------------------*/
  var resetReminder = function(){
    $reminderCheckbox.prop(&quot;checked&quot;,false);
    $reminderCheckbox.val(false);
    $timepicker.hide();    
  };

  /*---------------------------------------------------------------------*/
  //FUNCTION TO ENABLE REMINDER FIELDS - REMINDER NOT ACTIVE YET
  /*---------------------------------------------------------------------*/
  var enableReminderFields = function(){
    $reminderCheckbox.prop(&quot;disabled&quot;, false);
    $timePickerField.prop(&quot;disabled&quot;, false);
  };

  /*---------------------------------------------------------------------*/
  //FUNCTION TO DISABLE &amp; HIDE REMINDER FIELDS - SOME VALIDATION ISSUE
  /*---------------------------------------------------------------------*/
  var disableReminderFields = function(){
    $reminderCheckbox.prop(&quot;disabled&quot;, true);
    $reminderCheckbox.prop(&quot;checked&quot;, false);
    $timePickerField.prop(&quot;disabled&quot;, true);
    $timepicker.hide();
  };

  /*---------------------------------------------------------------------*/
  //FUNCTION TO DISABLE &amp; SHOW REMINDER FIELDS - (REMINDER SENT)
  /*---------------------------------------------------------------------*/
  var activeButDisabledReminderFields = function(){
    $reminderCheckbox.prop(&quot;disabled&quot;, true);
    $reminderCheckbox.prop(&quot;checked&quot;, true);
    $timePickerField.prop(&quot;disabled&quot;, true);
    $timepicker.show();
  };

  /*---------------------------------------------------------------------*/
  //FUNCTION TO ENABLE &amp; SHOW REMINDER FIELDS - (REMINDER ALREADY ACTIVE)
  /*---------------------------------------------------------------------*/
  var activeAndEnabledReminderFields = function(){
    $reminderCheckbox.prop(&quot;disabled&quot;, false);
    $timePickerField.prop(&quot;disabled&quot;, false);
    $reminderCheckbox.prop(&quot;checked&quot;, true);
    $timepicker.show();
  };

  /*---------------------------------------------------------------------*/
  //FUNCTION TO REMOVE ERROR DISPLAYED ON REMINDER TIME FIELD
  /*---------------------------------------------------------------------*/
  var markReminderTimeValid = function(){
    $timePickerField.removeAttr(&quot;style&quot;);
    showReminderNote(messageTypes.info, reminderMessages.ReminderInfo);
  }

  /*---------------------------------------------------------------------*/
  //FUNCTION TO DISPLAY ERROR ON REMINDER TIME FIELDS
  /*---------------------------------------------------------------------*/
  var markReminderTimeInvalid = function(){
    $timePickerField.css(&quot;border-color&quot;,&quot;red&quot;);
    showReminderNote(messageTypes.error, reminderMessages.InvalidReminderTime);
  }

  /*---------------------------------------------------------------------*/
  //ALL NOTES/MESSAGES RELATED FUNCTIONS
  /*---------------------------------------------------------------------*/
  var showReminderNote = function(type, msg){
    $.each(messageTypes,function(k,v){
      $reminderMsg.removeClass(v);
    });
    $reminderMsg.addClass(type);
    
    if(type == &quot;alert-warning&quot;)
    {
      $reminderTitleText.text(&quot;Warning:&quot;);
      $('#submit').attr(&quot;disabled&quot;, false);
      $('#submit').removeClass(&quot;btnDisabled&quot;);
      $('.beforeSubmit').css(&quot;display&quot;,&quot;none&quot;);
    }
    else 
    {
      if(type == &quot;alert-danger&quot;)
      {
        $('#submit').attr(&quot;disabled&quot;, true);
        $('#submit').addClass(&quot;btnDisabled&quot;);
        
        $('.beforeSubmit').css(&quot;display&quot;,&quot;block&quot;);
        $reminderTitleText.text(&quot;Error:&quot;);
      }
      else{      
        $('#submit').attr(&quot;disabled&quot;, false);
        $('#submit').removeClass(&quot;btnDisabled&quot;);
        $('.beforeSubmit').css(&quot;display&quot;,&quot;none&quot;);
        $reminderTitleText.text(&quot;Please note:&quot;);
      }
    }

    $reminderMsgText.text(msg);

    if($reminderMsg.hasClass(&quot;hide&quot;)){
      $reminderMsg.removeClass(&quot;hide&quot;);
    }
  }

  var hideReminderNote = function(){
    if(!$reminderMsg.hasClass(&quot;hide&quot;)){
      $reminderMsg.addClass(&quot;hide&quot;);
    }
  }

  var messageTypes = Object.freeze({
    &quot;error&quot;: &quot;alert-danger&quot;,
    &quot;warning&quot;: &quot;alert-warning&quot;,
    &quot;success&quot;: &quot;alert-success&quot;,
    &quot;info&quot;: &quot;alert-info&quot;
  });

  var reminderMessages = Object.freeze({
    &quot;AboutToStart&quot;: &quot;Reminder Time cannot be configured as the webcast is either about to start or start time has already passed.&quot;,
    &quot;ReminderInfo&quot;: &quot;Reminder time is dependent upon Start time. Kindly configure presentation Start Time first. Reminder time must be greater than current time and at least 5 minutes prior to the Webcast Start Time.&quot;,
    &quot;ReminderSent&quot;: &quot;Reminder has already been sent for this webcast at &quot;,
    &quot;PredefinedMsg&quot;:&quot;Email reminders can be sent to only registered users and cannot be setup for webcasts with pre-defined sign in registration.&quot;,
    &quot;PreLiveMessage&quot;:&quot;Email Reminders cannot be configured as Webcast is not in Pre-live or live state.&quot;,
    &quot;RegRequiredMsg&quot;: &quot;Email Reminders cannot be configured as Registration is not required for this webcast.&quot;,
    &quot;InvalidReminderTime&quot;: &quot;Invalid Reminder Time! Reminder time must be greater than current time &amp; less than the Webcast Start Time.&quot;
  });

  $(&quot;#lastSentEmailRemindersAccordian&quot;).on(&quot;show&quot;, function(e){
      $(e.target).prev().find('i').removeClass(&quot;icon-chevron-down&quot;).addClass(&quot;icon-chevron-up&quot;);
  });
  $(&quot;#lastSentEmailRemindersAccordian&quot;).on(&quot;hide&quot;, function(e){
      $(e.target).prev().find('i').removeClass(&quot;icon-chevron-up&quot;).addClass(&quot;icon-chevron-down&quot;);
  });


 
  

  
    Favicon upload
  

  
    Favicon Image
    
      

      
        
      

    
  

  
    
      
    
  
  
  
    Audio images upload
  

  
    Presentation audio images
    
      

      
        
      

    
  

  
    
      
        
      
    
  

  
    Slides upload
  

  
    Use 16:9 optimised conversion
    
      
      If results are not acceptable try standard conversion
    
  

  
    $(document).ready(function(){
      $('#useWideScale').on('click', function(event){
        var checked = 0;
        if ($(this).attr(&quot;checked&quot;) == &quot;checked&quot;){
          checked = 1;
        }
        $.ajax(&quot;/investis/clients/qapresentation/presentations/5e5cd0e01916330a00583e49&quot;, {
          type: &quot;PUT&quot;,
          dataType: &quot;json&quot;,
          data: {
            presentation: {
              useWideScale: checked
            }
          },
          error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus);
          }
        });
      });
    });
  

  
    Presentation PDF slides
    
      
      
        
      

      An error occurred processing the slides. Please try again.
    
  

  
    
       Processing slide PDF file
      
        
      
    
  

  
    
       Processing slide PDF file
      
        
      
    
  


  
    Current Slides
    Map Slides
  

  

    

      
        Current Slides
      

      

  
  

      
        Map Slides
        Update

      
      
        Image Mapping: 
        In order that add clickable links to the slide, you need to map the area of the image.
        Following section lets you add image areas to the respective slides.
        Directly add areas to the image, dont add map tag. Example:
        
          &lt;area shape=&quot;rect&quot; coords=&quot;148, 83, 332, 151&quot; href=&quot;http://www.google.com&quot; alt=&quot;ALT1&quot; title=&quot;TITLE1&quot; target=&quot;_blank&quot;/>
        
        
        
          Map feature will only load the updated (saved) mappings to the UI. Otherwise, it will be blank.
        
        

      
      
        
          Please export your presentations as PDF and use the slide uploader above
        
      
  


  $(&quot;#edit-form&quot;).submit(function(){
    var maps = [];
    $(&quot;.imagemap&quot;).each(function(e,d){
      maps.push({id: d.name, value:d.value});
    });
    var input = $(&quot;&lt;input>&quot;)
               .attr(&quot;type&quot;, &quot;hidden&quot;)
               .attr(&quot;name&quot;, &quot;presentation[imageMap]&quot;).val(JSON.stringify(maps));
    $(&quot;#edit-form&quot;).append($(input));
  });

  

  
    Stream settings 
  

  
    Stream Type
    
      
        Audio Only
        Video Only
        Video and Audio
      
      
        Choose between an audio only presentation or presentation with a video feed.
      
    
  
   
    Live Stream Protocol (Primary)
    
      
        RTMP
        HLS
      
      
        Default : HLS 
        RTMP Support has been removed. Only HLS is available for streaming.
        Note:HLS stream will have a higher delay then RTMP.
      
    
  


  
    FMLE profile
  

  
    
      
        Android Streaming
        Please note that the audio encoding option in FMLE must be set to AAC. MP3 audio does not play on Android devices
      

      
        Flash Media Live Encoder Profile
        profile.xml
      
      
        Right click and save &quot;profile.xml&quot;. File > Open > profile.xml in FMLE and away you go!
      
    
  

  
    Wirecast profile
  

   
  
    
      
        Wirecast Streaming
        Please note that we need to have two output in wirecast - RTMP Server type - one for Audio and other for Video.
          If its a audio webcast then only Audio profile is needed.
          We have to enter the authentication details individually in each stream using &quot;Set Credentials...&quot;
      
      
        Audio Profile
        profile-audio.xml
      
      
        Right click and save &quot;profile-audio.xml&quot;. Wirecast > Output > Output Settings > RTMP > Open FMLE XML File > profile-audio.xml
      
    
  
  


  
  
    Update
    Cancel
  


  
    
      Media uploads upload video and audio files for on-demand playback
    

    
      Media Encoding
      In order that your on-demand presentation plays on as many devices as possible you must use the encoding profiles below.
      Max file size limit: 5gb 
      
        
          Video

          
            *.mp4 file
            H.264
            320x240 (unless player interface uses different aspect ratio)
            25 fps (constant frame rate)
            Constant quality rate factor: 20 (alternative a bitrate is 300-400kbps)
            Baseline
            3.0
          
        

        
          Audio

          
            *.m4a
            AAC-LC
            44.1 Khz
            Mono
            56 kbps
          
        
      

    

    
      File Upload
    
    
      
        Current Video
        
          
            
          
        
      

      
        Upload/replace video
        
          
        
      
      
    

    
      
        Current Audio
        
          
            
              
            
          
        
      
    
      
        Upload/replace audio
        
          
        
      
    


    
      Media Process
    
  
  
    Generate HLS versions of media and thumbnails for video preview.
    NOTE: First upload necessary media and then process.
  
  
  
    
      
        
          HLS Options
        
      
      
        
          Video Size
          
            
            Output size of the video, Default : 448x256
          
          Video Frame Duration
          
            
            Chunks Duration, Default: 10 
          
          Bitrate
          
            
            Bitrate of audio for Audio only webcasts, Default: 128k
          
        
      
    
    
      
        
          Thumbnails Option
        
      
      
        
          Thumbnail Width
          
            
            Thumbnails width size(in px) displayed in player, Default: 100
          
          Number Of Thumbnails
          
            
            Number of thumbnails to be generated, Default: 6
          
          Spritesheet
          
            
            Spritesheet Formation for thumbnails,
            true - spritesheet(default), false - individual images
          
        
      
    
  


  
    Process Media
    
      Start
      Reset
    

  

  
    
      
          The media has been processed Successfully.
      
      
          The media is being processed... 
        
      
      
        The media processing has encountered error. Please Try again.
        
      
      
    
  





  // create the editor
  var starturl = &quot;/investis/clients/qapresentation/presentations/5e5cd0e01916330a00583e49/qacpresentation/startVideoProcessing&quot;;
  var reseturl = &quot;/investis/clients/qapresentation/presentations/5e5cd0e01916330a00583e49/qacpresentation/resetVideoProcessing&quot;;
  var processingState = &quot;&quot;;
  switch(processingState){
    case &quot;final&quot;:
      $(&quot;#mediaSuccess&quot;).show();
      $(&quot;#videoGif&quot;).hide();
      $(&quot;#mediaProgress&quot;).hide();
    case &quot;&quot;:
    case &quot;error&quot;:
      //not started
      $(&quot;#startmediaproc&quot;).removeAttr(&quot;disabled&quot;);
      break;
    default:
      //progress
      $(&quot;#mediaProgress&quot;).show();
      $(&quot;#startmediaproc&quot;).attr(&quot;disabled&quot;,&quot;disabled&quot;);
      $(&quot;#videoGif&quot;).show();
      break;

  }
  
  $(&quot;#startmediaproc&quot;).off().on(&quot;click&quot;, function(e){
    var data = {
      &quot;hsize&quot;: $(&quot;#hsize&quot;).val() || &quot;640x360&quot;,
      &quot;htime&quot;: $(&quot;#htime&quot;).val() || &quot;10&quot;,
      &quot;hbitrate&quot;: $(&quot;#hbitrate&quot;).val() || &quot;128k&quot;,
      &quot;twidth&quot;: $(&quot;#twidth&quot;).val() || &quot;100&quot;,
      &quot;tnumber&quot;: $(&quot;#tnumber&quot;).val() || &quot;6&quot;,
      &quot;tspritesheet&quot;: $(&quot;#tspritesheet&quot;).val() || &quot;true&quot;
    }
    $.ajax({
      type:&quot;POST&quot;,
      data: data,
      url: starturl
    }).done(function(res) {
      console.log(&quot;started&quot;);
      $(&quot;#mediaProgress&quot;).show();
      $(&quot;#video-progress&quot;).html(&quot;Waiting ..&quot;);
      $(&quot;#startmediaproc&quot;).attr(&quot;disabled&quot;,&quot;disabled&quot;);
      $(&quot;#mediaFail&quot;).hide();
      $(&quot;#videoGif&quot;).show();
      $(&quot;#mediaSuccess&quot;).hide();
    }).fail(function(err) {
      $(&quot;#video-fail&quot;).html(err.responseText);
      $(&quot;#mediaProgress&quot;).hide();
      $(&quot;#mediaFail&quot;).show();
      $(&quot;#mediaSuccess&quot;).hide();
      $(&quot;#videoGif&quot;).hide();
      console.log(&quot;Error&quot;, err);
    });
    e.preventDefault();
  });

  $(&quot;#resetmediaproc&quot;).off().on(&quot;click&quot;, function(e){
    var data = {
      &quot;hsize&quot;: $(&quot;#hsize&quot;).val() || &quot;640x360&quot;,
      &quot;htime&quot;: $(&quot;#htime&quot;).val() || &quot;10&quot;,
      &quot;hbitrate&quot;: $(&quot;#hbitrate&quot;).val() || &quot;128k&quot;,
      &quot;twidth&quot;: $(&quot;#twidth&quot;).val() || &quot;100&quot;,
      &quot;tnumber&quot;: $(&quot;#tnumber&quot;).val() || &quot;6&quot;,
      &quot;tspritesheet&quot;: $(&quot;#tspritesheet&quot;).val() || &quot;true&quot;
    }
    $.ajax({
      type:&quot;POST&quot;,
      data: data,
      url: reseturl
    }).done(function(res) {
      $(&quot;#mediaProgress&quot;).hide();
      $(&quot;#startmediaproc&quot;).removeAttr(&quot;disabled&quot;);
      $(&quot;#video-fail&quot;).html(&quot;&quot;);
      $(&quot;#mediaFail&quot;).show();
      $(&quot;#mediaSuccess&quot;).hide();
      $(&quot;#videoGif&quot;).hide();
    }).fail(function(err) {
      console.log(&quot;Error&quot;, err);
    });
    e.preventDefault();
  });



  var showFileUploadByType = function (streamType) {
    if (streamType === &quot;video&quot;) {
      $(&quot;#uploadVideo&quot;).show();
      $(&quot;#uploadAudio&quot;).show();
    } else if (streamType === &quot;videoonly&quot;) {
      $(&quot;#uploadVideo&quot;).show();
      $(&quot;#uploadAudio&quot;).hide();
    } else {
      $(&quot;#uploadAudio&quot;).show();
      $(&quot;#uploadVideo&quot;).hide();
    }
  }

  var strmtyp = &quot;audio&quot;;
  showFileUploadByType(&quot;audio&quot;)

  $(&quot;select[name='presentation[streamType]']&quot;).on('change', function () {
    showFileUploadByType(this.value);
  });



    
    
      
        
          
            Registration 
          
        
        
          
            
              
                
                  
                   Is registration/login required to view the presentation?
                
              
            

            
              
              
                Please note:  
              

              
                
                  
                     Open Registration
                    Anyone with the URL to the presentation will be able to register for or sign in to the presentation
                  
                  
                     Guest List Registration
                    Only people in the guest list will be able to register for or sign in to the presentation
                  
                  
                     Pre-defined Sign In
                    Single user name and password, no registration. Only people with the user name and password will be able to sign in to the presentation
                  
                
              

              
                
                  
                    
                      
                        
                          
                           Should the viewer provide a password?
                        
                        Upon registration a new viewer will be asked to select a password. The viewer will then be able to sign in using their email address and password for future presentations.
                      
                    
                  
                

                
                  
                    
                      
                         Should the viewer provide a password?
                      
                      Upon registration a new viewer will be asked to select a password. The viewer will then be able to sign in using their email address and password for future presentations.
                    
                    
                    
                        
                            Individual Emails:
                        
                    
                    
                      
                        
                      
                      
                        Enter a list of individual's email addresses. Each address must be on a new line or separated with a comma (Don't mix both) e.g.
                        person@domain.com, another@elsewhere.co.uk, who@where.com
                      
                    
                    
                        
                            Domains:
                        
                    
                    
                        
                          
                        
                        
                          Enter a list of domains. All users having emails with the domains entered in this field will be allowed to register. Each domain must be on a new line or separated with a comma (Don't mix both) e.g.
                          domain.com, somedomain.co.uk, company.com
                        
                      
                  
                

                
                  
                    
                      
                        
                          User name
                          
                        
                        
                          Password
                          
                        
                      
                      
                         Reports for this presentation will not be able to show usage for specific viewers. There is no way to differentiate between viewers when using a shared user name and password.
                      
                    
                  
                
              
            

            
              
                
                  
                   Custom form fields
                
              
            

            
              Registration Area : 
              
                
                  
                    &lt;div class=&quot;control-group&quot;>
  &lt;label for=&quot;registration-name&quot; class=&quot;control-label&quot;>Name&lt;/label>
 &lt;div class=&quot;controls&quot;>
   &lt;input id=&quot;registration-name&quot; name=&quot;user[metadata][name]&quot; type=&quot;text&quot; class=&quot;required&quot;>
 &lt;/div>
&lt;/div>

&lt;div class=&quot;control-group&quot;>
  &lt;label for=&quot;registration-company&quot; class=&quot;control-label&quot;>Company&lt;/label>
  &lt;div class=&quot;controls&quot;>
    &lt;input id=&quot;registration-company&quot; name=&quot;user[metadata][company]&quot; type=&quot;text&quot; class=&quot;required&quot;>
  &lt;/div>
&lt;/div>
                  
                  
                    Enter HTML form fields to build your form. Make sure you use the correct name attribute e.g.
                    &lt;input id=&quot;registration-country&quot; name=&quot;user[metadata][country]&quot; type=&quot;text&quot; class=&quot;&quot;>
                    Note: Please use input type=&quot;text&quot; for custom form fields.
                     Fill with example
                  
                  
                    $(function() {
                      $('#fill-custom-field').on('click', function() {
                        if (!confirm('This will overwrite the current custom fields.\n\nAre you sure?')) return false;
                        var exampleHTML = '&lt;div class=&quot;control-group&quot;>\n  &lt;label for=&quot;registration-name&quot; class=&quot;control-label&quot;>Name&lt;/label>\n\  &lt;div class=&quot;controls&quot;>\n\    &lt;input id=&quot;registration-name&quot; name=&quot;user[metadata][name]&quot; type=&quot;text&quot; class=&quot;required&quot;>\n\  &lt;/div>\n\&lt;/div>\n\n\&lt;div class=&quot;control-group&quot;>\n\  &lt;label for=&quot;registration-role&quot; class=&quot;control-label&quot;>Role&lt;/label>\n\  &lt;div class=&quot;controls&quot;>\n\    &lt;select name=&quot;user[metadata][occupation]&quot; class=&quot;required&quot;>\n\      &lt;option>Select an Occupation&lt;/value>\n\      &lt;option value=&quot;Buy-side analyst&quot;>Buy-side analyst&lt;/option>\n\      &lt;option value=&quot;Sell-side analyst&quot;>Sell-side analyst&lt;/option>\n\      &lt;option value=&quot;Fund Manager&quot;>Fund Manager&lt;/option>\n\      &lt;option value=&quot;Private investor&quot;>Private investor&lt;/option>\n\      &lt;option value=&quot;Media&quot;>Media&lt;/option>\n\      &lt;option value=&quot;Employee&quot;>Employee&lt;/option>\n\      &lt;option value=&quot;Other&quot;>Other&lt;/option>\n\    &lt;/select>\n\  &lt;/div>\n\&lt;/div>';

                        $('#customFieldsHTML').val(exampleHTML);
                        return false
                      });
                    });
                  
                
              
              Login Area : 
              
                
                  
                    
                  
                  
                    Login Area : Enter HTML form fields to build your form. Make sure you use the correct name attribute [login[metadata][count]]e.g.
                    &lt;input id=&quot;registration-country&quot; name=&quot;login[metadata][count]&quot; type=&quot;text&quot; class=&quot;&quot;>

                    Note: Please use input type=&quot;text&quot; for custom form fields.
                     Fill with example
                  
                  
                    $(function() {
                      $('#fill-custom-field2').on('click', function() {
                        if (!confirm('This will overwrite the current custom fields.\n\nAre you sure?')) return false;
                        var exampleHTML = '&lt;div class=&quot;control-group&quot;>\n  &lt;label for=&quot;viewer-count&quot; class=&quot;control-label&quot;>Number Of Viewers&lt;/label>\n\  &lt;div class=&quot;controls&quot;>\n\    &lt;input id=&quot;viewer-count&quot; name=&quot;login[metadata][count]&quot; type=&quot;number&quot;>\n\  &lt;/div>\n\&lt;/div>\n\n';

                        $('#customFieldsHTMLLogin').val(exampleHTML);
                        return false
                      });
                    });
                  
                
              
            

            
              
                
                  
                   Only allow certain IP addresses to view the presentation?
                
              
              
                
                  
                    Please separate each address with a comma or new line
                    
                  
                
              
            
          
        
      
      
        
          
            Disclaimer 
          
        
        
          
            
              Display Disclaimer?
              
                
                  
                   Should viewers be shown a disclaimer page before viewing the presentation?
                
              
              
                
                  
                   Hide the decline button? Checking this box means viewers will only see an 'accept' button
                
              
            

          
        
      
      
        
          
           Notes 
          
        
        
          
            
              Display Notes?
              
                
                  
                   Should viewers be able to store Notes?
                
              
            
          
        
      
      
        
          
            Video Sharing 
          
        
        
          
            
              Display Share Button?
              
                
                  
                   Should viewers be able to share the webcast through share button on video?
                
              
            
          
        
      

      
        
          
            Media Download 
          
        
        
          
            
              Download
              
                
                  
                   Should the media be downloadable?
                
              
            
          
        
      
      
        
          
           Questions and Feedback 
          
        
        
          
            
              Allow questions:
              

                
                  
                   before the presentation?
                

                
                  
                   during the presentation?
                

               
                
                  
                   Should viewers be able to view live questions  during presentation?
                
                

              
              Allow feedback?
              
                
                  
                   Should viewers be able to provide feedback during the live presentation and while viewing the on-demand presentation?
                
              
              
            
          
        
      
      
        
          
            Cookie Usage Alert 
          
        
        
          
            
              Display Cookie Alert?
              
                
                  
                   Should the Cookie Usage Alert be shown to users? (Default : true)
                
              
            
          
        
      

      
        
          
            Viewer Chat 
          
        
        
          
            
              Display Viewer Chat Popup?
              
                
                  
                   Should the viewers be able to chat in the webcast? (Default : false)
                
                Make sure you have password enabled while using the chat. This helps in keeping user privacy intact.
              
            
            
              Delete Viewer Chat?
              
                 
                  Delete
                  You wont be able to recover it again.
                
              
            
          
        
      


      
      
      
      
      
      

      
          
            
              Email Customization 
            
          
          
            
                
                    
                      
                         Default Email Template
                      
                      
                         Custom Email Template
                      
                    
                


                                
                  
                      Dear #User's email here#, 
                          The QA_Presentation - QA_CPresentation live webcast will start at 09:24 GMT on 2nd Mar 2020 . 
                          Click on the button below to access.  
                          
                          Live Webcast 
                             
                          If the button doesn't work, copy &amp; paste this link into your browser: 
                          https://staging.investis-live.com/qapresentation/5e5cd0e01916330a00583e49/qacpresentation 
                            
                          Thank you, 
                          The Webcasting team 
                           
                           
                           
                          This email was sent to #User's email here# 
                          You received this email because you registered to the QA_Presentation webcast.  
                          Please click here to unsubscribe from this specific webcast.
                              
                      
                

                                
                  
                    
                      
                    Subject
                    
                      
                    

                    Title
                    
                        #User's email gets added here#,
                    


                    Header
                    
                        
                                        
                     
                    Webcast Button Text
                    
                      
                    
                    
                    Webcast Link Text
                    
                      
                    

                    
                    
                      https://staging.investis-live.com/qapresentation/5e5cd0e01916330a00583e49/qacpresentation
                    
                    
                    

                    Footer
                    
                        
                    
                    
                    Unsubscribe Link Text
                    
                        
                      
                    

                    
                    
                      
                    
                    
                      
                    
                    
                      Show Preview
                      Hide Preview
                    
                    
                  
                
            
                
          
        
    

    
      Update
      Cancel
    


    
      $(&quot;#audienceAccordian&quot;).on(&quot;show&quot;, function(e){
          $(e.target).prev().find('i').removeClass(&quot;icon-chevron-down&quot;).addClass(&quot;icon-chevron-up&quot;);
      });
      $(&quot;#audienceAccordian&quot;).on(&quot;hide&quot;, function(e){
          $(e.target).prev().find('i').removeClass(&quot;icon-chevron-up&quot;).addClass(&quot;icon-chevron-down&quot;);
      });

      $(&quot;#viewerchatdelete&quot;).on(&quot;click&quot;, function(e){
          e.preventDefault();
          var sure = prompt(&quot;Are you sure you want to delete the chat ? Type \&quot;yes\&quot; to continue&quot;);
          if(sure &amp;&amp; sure.toLowerCase() === &quot;yes&quot;){
            $.ajax({
              type:&quot;GET&quot;,
              url: &quot;/investis/clients/qapresentation/presentations/5e5cd0e01916330a00583e49/removeChat&quot;
            }).done(function(res) {
              alert(&quot;Removed Successfully&quot;);
            }).fail(function(res) {
              alert(&quot;Something went wrong, try again later&quot;);
              console.log(&quot;Error&quot;, res);
            }); 
          }
      });
     
    
  

  

  
    Registration
    Disclaimer
    Pre-live
    Post-live
    Header
  

  

    

      

        
           HTML Content
        

        
          

            
              
            

            
              This needs to be HTML e.g.
              
                
                  &lt;h3>Heading&lt;/h3>
                  For headings
                
                
                  &lt;p>Paragraph&lt;/p>
                  For paragraph text
                
                
                  &lt;a href=&quot;url&quot;>Text&lt;/a>
                  If you need to link to anything
                
              
            

          
        
      

    

    

      

        
           HTML Content
        

        
          

            
              
            

            
              This needs to be HTML e.g.
              
                
                  &lt;h3>Heading&lt;/h3>
                  For headings
                
                
                  &lt;p>Paragraph&lt;/p>
                  For paragraph text
                
                
                  &lt;a href=&quot;url&quot;>Text&lt;/a>
                  If you need to link to anything
                
              
            

          
        
      

      
        
           Declined content
        

        
          

            
              
            

            
              This content will be shown to viewers if they choose to not accept the disclaimer.
              You can choose to hide the 'decline' button in the audience tab
            

          
        
      

    

    

      
         HTML Content
        
          
            
              
            
              This needs to be HTML e.g.
              
                
                  &lt;h3>Heading&lt;/h3>
                  For headings
                
                
                  &lt;p>Paragraph&lt;/p>
                  For paragraph text
                
                
                  &lt;a href=&quot;url&quot;>Text&lt;/a>
                  If you need to link to anything
                
              
            
          
        
      

    

    

      
         HTML Content
        
          
            
              
            
            
              This needs to be HTML e.g.
              
                
                  &lt;h3>Heading&lt;/h3>
                  For headings
                
                
                  &lt;p>Paragraph&lt;/p>
                  For paragraph text
                
                
                  &lt;a href=&quot;url&quot;>Text&lt;/a>
                  If you need to link to anything
                
              
            
          
        
      

    

    
    
        
           HTML Content
          
            
              
                
              
              
                This can be HTML e.g.
                
                  
                    &lt;p>Paragraph&lt;/p>
                    For paragraph text
                  
                  
                    &lt;a href=&quot;url&quot;>Text&lt;/a>
                    If you need to link to anything
                  
                
              
            
          
        
    
      

  

  
    Update
    Cancel
  


  
  
    ×
      Sorry you are using a '&quot;' in the name of your tab which is not a valid character. Please use something different or remove it.
  
  
    
      &lt;li class=&quot;{{ active ? 'active' : ''}}&quot;>&lt;a href=&quot;#{{ escapedName }}&quot; data-toggle=&quot;tab&quot;>{{ name }}&lt;/a>&lt;/li>
    
    
      Downloads
    + Add another
  

  
    
      &lt;input type=&quot;hidden&quot; name=&quot;content[{{ name }}][id]&quot; value=&quot;{{ _id }}&quot;>
      &lt;div class=&quot;control-group&quot;>
        &lt;input type=&quot;hidden&quot; name=&quot;content[{{ name }}][name]&quot; value=&quot;{{ name }}&quot;>
        &lt;label class=&quot;control-label&quot;>Tab is visible?&lt;/label>
        &lt;div class=&quot;controls&quot;>
          &lt;label class=&quot;checkbox&quot;>&lt;input type=&quot;checkbox&quot; name=&quot;content[{{ name }}][visible]&quot; {{ (visible) ? &quot;checked&quot; : '' }}>&lt;/label>
        &lt;/div>
      &lt;/div>
      &lt;div class=&quot;control-group&quot;>
        &lt;label class=&quot;control-label&quot;>Content&lt;/label>
        &lt;div class=&quot;controls&quot;>
          &lt;textarea name=&quot;content[{{ name }}][content]&quot; rows=&quot;18&quot; class=&quot;resize vertical span6&quot; placeholder=&quot;HTML friendly content area&quot;>{{ content.replace(/&lt;\\\//ig, '&lt;/') }}&lt;/textarea>
        &lt;/div>
        &lt;div class=&quot;controls&quot;>
          &lt;button type=&quot;button&quot; class=&quot;btn btn-default addVideoCode&quot;>Add video code&lt;/button>
        &lt;/div>
          
      &lt;/div>
      &lt;div class=&quot;controls control-group&quot;>
        &lt;ul id=&quot;{{ name.replace(/\'/g, '-').replace(/\s/g, '-') }}-downloads-list&quot; class=&quot;downloads&quot;>
          {% for(var i = 0; i &lt; assets.length; i++) { %}
            &lt;li data-extend=&quot;removeable&quot; data-endpoint=&quot;../removeAsset&quot; data-type=&quot;contentAsset&quot; data-id=&quot;{{ assets[i]._id }}&quot;>&lt;a class=&quot;asset&quot; href=&quot;{{ assets[i].path }}&quot;>{{ assets[i].name }}&lt;/a>&lt;/li>
          {% } %}
        &lt;/ul>
      &lt;/div>
      &lt;div class=&quot;control-group&quot;>
        &lt;label for=&quot;file&quot; class=&quot;control-label&quot;>Item&lt;/label>
        &lt;div class=&quot;controls&quot;>
          &lt;input id=&quot;download&quot; name=&quot;content[{{ name }}]&quot; type=&quot;file&quot; data-extend=&quot;auto-upload&quot; data-upload-url=&quot;/investis/clients/qapresentation/presentations/5e5cd0e01916330a00583e49/uploadAsset&quot; data-repeat=&quot;true&quot; data-prependto=&quot;#{{ name.replace(/\'/g, '-').replace(/\s/g, '-') }}-downloads-list&quot; data-preview-images=&quot;false&quot;>
        &lt;/div>
      &lt;/div>
      &lt;div class=&quot;control-group&quot;>
        &lt;div class=&quot;controls&quot;>
        &lt;form action=&quot;/investis/clients/qapresentation/presentations/5e5cd0e01916330a00583e49/tabs/{{ _id }}&quot;, method=&quot;POST&quot;>
          &lt;input type=&quot;hidden&quot; name=&quot;_method&quot; value=&quot;DELETE&quot;>
          &lt;input type=&quot;submit&quot; class=&quot;btn btn-danger&quot; value=&quot;Remove tab&quot;/>
        &lt;/form>

        &lt;/div>
      &lt;/div>
    
  
      
      
        
        Tab is visible?
        
          
        
      
      
        Content
        
          
        
        
          Add video code
        
          
      
      
        
          
        
      
      
        Item
        
          
        
      
      
        
        
          
          
        

        
      
    

  
    Update
    Cancel
  



  
  

   
    Online
    Upload
  

  
    
        
          Translations: Fill Online
        

        
            powered by aceXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
        
         Fill Default
         Download
    
    

      
        Translations: Upload File
      

      
        
          
            To use a translated copy for the pages in the presentation, you'll need to download the copy file, change the copy inside of it, and then upload that file via the form below.
            
              Download the default copy file
            
            Important! Do not delete any fields from the JSON file, or modify the key names, and make sure that the JSON file is valid before you upload it. You can check that the file is valid here.
            If you wish to use the default copy for the presentation and not a translated copy, upload the default copy file.
          
        

      

      
      
        Current File
        
          
            
              No File Uploaded.
            
          
          
            Deprecated
            Please download the file from online editor as that is latest. This might not contain the changes made there. This functionality will be removed in next release. 
          
        
      

      
        Upload/replace File
        
         
        
      
    
  

  
    Update
    Cancel
  
 



  // create the editor
  var container = document.getElementById(&quot;jsoneditor&quot;);
  var options = {
    mode: &quot;code&quot;
  };
  var editor = new JSONEditor(container, options);

  

  $(&quot;#edit-form&quot;).submit(function(){
    var input = $(&quot;&lt;input>&quot;).attr(&quot;type&quot;, &quot;hidden&quot;).attr(&quot;name&quot;, &quot;presentation[copy]&quot;).val(JSON.stringify(editor.get()));
    $(&quot;#edit-form&quot;).append($(input));
  });

  $(&quot;#fillDefault&quot;).off().on(&quot;click&quot;, function(){
    $.ajax({
      type:&quot;GET&quot;,
      url: &quot;/js/copyFile.json&quot;
    }).done(function(res) {
      editor.set(res);
    }).fail(function(res) {
      console.log(&quot;Error&quot;, res);
    });
  })

  $(&quot;#downloadTranslation&quot;).off().on(&quot;click&quot;, function(){
    var blob = new Blob([editor.getText()], {type: 'application/json;charset=utf-8'});
    saveAs(blob, &quot;translation-qacpresentation.json&quot;);
  });


  
  
    Display Results?
    
      
        
         Do we need to show results to viewers?
      
      
        Please note: If you want to update the visibility of the polls results once the presentation has been started, please refresh the page before checking/unchecking this box.
      
    
  
  
    
      &lt;li class=&quot;{{ active ? 'active' : ''}}&quot;>&lt;a href=&quot;#{{ escapedName }}&quot; data-toggle=&quot;tab&quot;>{{ name }}&lt;/a>&lt;/li>
    
    + Add new poll
  

  
    
      &lt;div id=&quot;pollCont_{{ name.replace(/\'/g, '-').replace(/\s/g, '-') }}&quot;>
        &lt;input type=&quot;hidden&quot; class=&quot;currentPoll&quot; name=&quot;polls[{{ name }}][id]&quot; value=&quot;{{ _id }}&quot;>
        &lt;div class=&quot;control-group&quot;>
          &lt;input type=&quot;hidden&quot; name=&quot;polls[{{ name }}][name]&quot; value=&quot;{{ name }}&quot;>
          &lt;label class=&quot;control-label&quot;>Poll is visible?&lt;/label>
          &lt;div class=&quot;controls&quot;>
            &lt;label class=&quot;checkbox&quot;>&lt;input type=&quot;checkbox&quot; name=&quot;polls[{{ name }}][visible]&quot; {{ (question_is_visible) ? &quot;checked&quot; : '' }}>&lt;/label>
          &lt;/div>
        &lt;/div>
        &lt;div class=&quot;control-group&quot;>
          &lt;label class=&quot;control-label&quot;>Question&lt;/label>
          &lt;div class=&quot;controls&quot;>
            &lt;input type=&quot;text&quot; name=&quot;polls[{{ name }}][question]&quot; class=&quot;resize vertical span6 questionBox&quot; placeholder=&quot;Question here&quot; value=&quot;{{ question.replace(/&quot;/g, '&amp;quot;').replace(/'/g, '&amp;#39;') }}&quot;/>
          &lt;/div>
        &lt;/div>
        &lt;div class=&quot;control-group&quot;>
        &lt;label class=&quot;control-label&quot;>Answers&lt;/label>
          &lt;div class=&quot;controls answersParent&quot;>
          &lt;ul id=&quot;{{ name.replace(/\'/g, '-').replace(/\s/g, '-') }}-answers-list&quot; class=&quot;answers&quot;>
            {% for(var i = 0; i &lt; answers.length; i++) { %}
                &lt;li data-id=&quot;{{ answers[i]._id }}&quot; class=&quot;input-append&quot;>&lt;input type=&quot;text&quot; class=&quot;answer span5&quot; data-count=&quot;{{ answers[i].count }}&quot; value=&quot;{{ answers[i].value.replace(/&quot;/g, '&amp;quot;').replace(/'/g, '&amp;#39;') }}&quot;/>&lt;cross class=&quot;btn&quot;>&lt;i class=&quot;icon-remove-sign&quot;>&lt;/i>&lt;/cross>&lt;/li>

            {% } %}

              &lt;li data-id=&quot;0&quot; class=&quot;input-append&quot;>&lt;input type=&quot;text&quot; class=&quot;answer span5&quot; placeholder=&quot;New answers&quot; data-count=&quot;-1&quot; value=&quot;&quot; />&lt;cross class=&quot;btn&quot;>&lt;i class=&quot;icon-remove-sign&quot;>&lt;/i>&lt;/cross>&lt;/li>
              &lt;/ul>
              &lt;input type=&quot;button&quot; class=&quot;answersAdd btn btn-info&quot; value=&quot;Add new answer&quot; />
          &lt;/div>
        &lt;/div>
        &lt;div class=&quot;control-group&quot;>
          &lt;div class=&quot;controls&quot; style=&quot;margin-top:10px;&quot;>
              &lt;input type=&quot;button&quot; data-id=&quot;{{ _id }}&quot; id=&quot;removePoll_{{ _id }}&quot; class=&quot;btn btn-danger removePoll&quot; value=&quot;Remove Poll&quot;/>
          &lt;/div>
        &lt;/div>
      &lt;/div>
    
  

  
    Update
    Cancel
  



  
  
    Enable in .myInvestis
  
  
      
        
          
           Show this webcast in myInvestis
        
      
    
    
      Present View
    
    
      
        
        
          
           Enable Record Button
        
        
          
           Enable Broadcast Message Button
        
        
          
           Enable Webcast State dropdown
        
      
    
    
      Update
      Cancel
    




  

  
    ×
    Upload a New Theme
  

  

  
  

  
    ×
    Images in your CSS file should be referenced asbackground-image: url('./images/{image_name.png}');
  

  

    
      Name
      
    

  

  
    
          CSS file
          
              
          
        

        
          CSS images
          

        

              You can choose multiple files in one go as long as you're using a modern browser! Just highlight all your images in the file dialog.

        
      
        

      

  

  
    
  

  












var $registraionMsgText = $(&quot;#registraionTypeMsg .msg&quot;);
var $registraionMsgTitle = $(&quot;#registraionTypeMsg .note&quot;);
  var port = (window.location.protocol == &quot;https:&quot;) ? 443 : 80;
  if ('WebSocket' in window &amp;&amp; window.WebSocket.CLOSING === 2) {
    var socketConnection = socketCluster.connect({
      hostname: config.pubub.websocketsSupported.hostname,
      port: port
    });
  } else {
    var socketConnection = sc1.connect({
      hostname: config.pubub.websocketsNotSupported.hostname,
      port: port
    });
  }

  var socket = socketConnection.subscribe('5e5cd0e01916330a00583e49');

  var user = {
    id: &quot;5beadfe8fa81ca678e5d2a98&quot;,
    role: &quot;owner&quot;
  };
  var cleared = false;
  var pdfEventFunctions = {
    slideProcessed: function(data) {
      if (!cleared) {
        $('#current-slides ul.thumbnails').empty();
        cleared = true;
      }
      data.thumbUrl = data.thumbUrl.replace(&quot;http://&quot;,&quot;https://&quot;);
      $('#current-slides ul.thumbnails').append('&lt;li data-index=&quot;' + data.index + '&quot;>&lt;img class=&quot;thumbnail&quot; src=&quot;' + data.thumbUrl + '&quot; width=&quot;210&quot; height=&quot;158&quot;>&lt;/li>');

      var $wrapper = $('#current-slides ul.thumbnails');

      $wrapper.find('li').sort(function (a, b) {
        x = a;
        y = b;
          return +a.getAttribute('data-index') - +b.getAttribute('data-index');
      }).appendTo($wrapper);
    },
    error: function(data) {
      $('#current-slides ul.thumbnails').empty();
      $('#pdf-processing').html(&quot;An error occurred processing the slides. Please try again.&quot;);
    },
    complete: function(data) {
      $('#pdf-processing').html(&quot;Processing is complete&quot;);
    }
  };


  socket.watch(function(data) {
    var dataObject = data;

    if (dataObject.event === 'pdfProcessingState') {

    var cargo = dataObject.message;

    if (cargo.event != undefined) {
      pdfEventFunctions[cargo.event](cargo.data);
    } else if (cargo.state != undefined) {
      var state = cargo.state;

      if(state == &quot;processing&quot;) {
        $('#pdf').addClass('hide');
        $('#pdf-processing').removeClass('hide');
      }

      if(state == &quot;complete&quot;) {
        $('#slide-progress').addClass('hide');
        $('#thumb-progress').addClass('hide');
        $('#pdf-processing').addClass('hide');
        $('#pdf').removeClass('hide');
      }

    };
  }else if(dataObject.event === 'videoProcessing'){
    var processingState = dataObject.message.event;
    $(&quot;#video-progress&quot;).html(processingState);
    $(&quot;#mediaFail&quot;).hide();
    switch(processingState){
      case &quot;final&quot;:
        $(&quot;#startmediaproc&quot;).removeAttr(&quot;disabled&quot;);
        $(&quot;#mediaSuccess&quot;).show();
        $(&quot;#videoGif&quot;).hide();
        $(&quot;#mediaProgress&quot;).hide();
        break;
      case &quot;&quot;:
        //not started
        $(&quot;#startmediaproc&quot;).removeAttr(&quot;disabled&quot;);
        break;
      case &quot;error&quot;:
        //not started
        $(&quot;#startmediaproc&quot;).removeAttr(&quot;disabled&quot;);
        $(&quot;#mediaFail&quot;).show();
        break;
      default:
        //progress
        $(&quot;#mediaSuccess&quot;).hide();
        $(&quot;#mediaProgress&quot;).show();
        $(&quot;#startmediaproc&quot;).attr(&quot;disabled&quot;,&quot;disabled&quot;);
        $(&quot;#videoGif&quot;).show();
        break;

    }
  }else if (dataObject.event === 'imageMap') {
    var code = dataObject.message.code;
    var slide = dataObject.message.slide;
    $(&quot;textarea[name='&quot;+slide+&quot;']&quot;).val(code);
  }


  });



//////////////////
// Audience Tab //
//////////////////



$('#registrationRequired').change(function() {
  $('#registrationRequiredControls').collapse('toggle');
  return true;
})

// To handle on initial view
var customFieldsSectionState = $('#customFields').attr('checked') ? &quot;show&quot; : &quot;hide&quot;;
$('#customFieldControls').collapse(customFieldsSectionState);

$('#specifyIPs').on('change', function() {
  var elementName = $(this).data('collapse');
  console.log(elementName)
  $(elementName).collapse('toggle');
});

$('#customFields').change(function() {
  var state = $(this).attr('checked') ? &quot;show&quot; : &quot;hide&quot;;
  $('#customFieldControls').collapse(state);
  return true;
})



// // Set up the collapse elements but don't auto-show them
$('#registrationFields, #guestlistEntry, #predefinedLoginFields').collapse({toggle:false});



// When the registration type is changed show and hide the correct collapse elements
$('[name=&quot;presentation[viewerAuthMethod]&quot;]').change(function() {
  var reminderActive = ($reminderCheckbox.val()==&quot;true&quot; || $reminderCheckbox.val()==true);
  if(reminderActive)
  {    
    $(&quot;#registraionTypeMsg&quot;).css(&quot;display&quot;,&quot;block&quot;);
  }
  else{
    $(&quot;#registraionTypeMsg&quot;).css(&quot;display&quot;,&quot;none&quot;);
  }
  if ($(this).is('#predefinedLogin'))
  {
    $('#registrationFields').removeClass('in');
    $('#guestlistEntry').removeClass('in');
    $('#predefinedLoginFields').addClass('in');
    
    $(&quot;#registraionTypeMsg&quot;).removeClass('alert-info'); 
    $(&quot;#registraionTypeMsg&quot;).addClass('alert-warning');
    
    $registraionMsgTitle.text(&quot;Warning:&quot;);
    $registraionMsgText.text(&quot;Email reminders setup for this webcast will be removed because the email reminder feature is not available for webcasts with pre-defined sign in registration.&quot;);
    
  } 
  else if ($(this).is('#guestlistLogin')) 
  {
    $('#registrationFields').removeClass('in');
    $('#predefinedLoginFields').removeClass('in');
    $('#guestlistEntry').addClass('in');
    
    $(&quot;#registraionTypeMsg&quot;).removeClass('alert-warning'); 
    $(&quot;#registraionTypeMsg&quot;).addClass('alert-info');
    $registraionMsgTitle.text(&quot;Please note:&quot;);
    $registraionMsgText.text(&quot;Email reminders if setup will be sent to only registered users from the guest list.&quot;);
  } 
  else if ($(this).is('#viewerLogin')) 
  {
    $('#predefinedLoginFields').removeClass('in');
    $('#guestlistEntry').removeClass('in');
    $('#registrationFields').addClass('in');

    /* $registraionMsgText.text(&quot;Email Reminders send to only viewer who registered - Open Registration Users.&quot;); */
    $(&quot;#registraionTypeMsg&quot;).css(&quot;display&quot;,&quot;none&quot;);
  }
});



$('#registrationPasswordRequired1').click(function() {
  if($(this).attr('checked')) {
    $('#registrationPasswordRequired2').attr('checked', true);
  } else {
    $('#registrationPasswordRequired2').attr('checked', false);
  }
});
$('#registrationPasswordRequired2').click(function() {
  if($(this).attr('checked')) {
    $('#registrationPasswordRequired1').attr('checked', true);
  } else {
    $('#registrationPasswordRequired1').attr('checked', false);
  }
});


  var emailCustomizationDOM = {
    inputSubject: '#emailSubject',
    inputTitle: '#emailTitle',
    inputHeader: '#emailHeader',
    inputFooter: '#emailFooter',
    inputPresentationLinkText: '#emailPresentationLinkText',
    inputPresentationLinkInfo: '#emailPresentationLinkInfo',
    inputUnsubscribeInfo: '#emailUnsubscribeInfo',
    hiddenSubject: '#hiddenEmailSubject',
    hiddenTitle: '#hiddenEmailTitle',
    hiddenHeader: '#hiddenEmailHeader',
    hiddenFooter: '#hiddenEmailFooter',
    hiddenPresentationLinkText: '#hiddenEmailPresentationLinkText', 
    hiddenPresentationLinkInfo: '#hiddenEmailPresentationLinkInfo',
    hiddenUnsubscribeInfo: '#hiddenEmailUnsubscribeInfo',
    showPreviewEmailTemplate: '#showPreviewEmailTemplate',
    cancelPreviewEmaiTemplate: '#cancelPreviewEmaiTemplate',
    previewEmaiTemplate: '#previewEmaiTemplate'
  };

  var emailCustomizatonMessages = Object.freeze({
    error: {
      unsubsubscribeKeyForm: 'Anchor tag in &amp;lt;a href=&quot;##&quot;&amp;gtLink text&amp;lt/a&amp;gt; format is mandatory for this field.',
      mainRequiredField: 'All fields under the Email customization section are mandatory, kindly fill out those fields to update successfully.',
      mainUnsubscribeKey: 'Anchor tag in &amp;lt;a href=&quot;##&quot;&amp;gtLink text&amp;lt/a&amp;gt; format for Unsubscribe Link Text in Email customization section are mandatory, fill it before update.',
      maxLengthValidation: 'Please enter no more than &lt;b>__allowedMaxLength__&lt;/b> characters for the &lt;b>__fieldName__&lt;/b> field under the email customization section.'
    }
  });

  var editFormValidationOptions = {
    focusCleanup: true,
    onfocusout: function (element) {
      $(element).valid();
      //Checking form validation on any field blur
      if($(&quot;#edit-form&quot;).valid() &amp;&amp; ($('#field-tabs li.active a').attr('href') == '#audience') &amp;&amp; $(&quot;#emailCustomizationErrorId&quot;).length != 0){
        //Setting delay to preserve tab click. Otherwise tab change doesn't work because it shifts up
        setTimeout(function(){$(&quot;#emailCustomizationErrorId&quot;).remove();},150);
      }
    },
    errorPlacement: function (error, element) {
      error.addClass('badge badge-important');
      element.parents('.controls').append(error);    
    }
  };

 //Generates a bootstrap alert element and returns html which can be appended on DOM
  var bootstrapAlertMessage = function (type, message, id, showCloseOption) {
    var elemId = id ? 'id=' + id : '';

    var html = '&lt;div ' + elemId + ' class=&quot;bootstrapAlertMessage alert alert-' + type + ' alert-dismissible&quot;>';
    if (showCloseOption) { html += '&lt;a href=&quot;#&quot; class=&quot;close&quot; data-dismiss=&quot;alert&quot; aria-label=&quot;close&quot;>&amp;times;&lt;/a>'; }
    html += message + '&lt;/div>';

    return html;
  };
  
  var unSubscribeInfoRegexPatt = /&lt;a.*(?=href=\&quot;\##&quot;|\'\##')[^>]*>([^&lt;]+)&lt;\/a>/;

  $.validator.addMethod('unsubscribeInfoReguired', function (value) {
       return unSubscribeInfoRegexPatt.test(value);
    }, emailCustomizatonMessages.error.unsubsubscribeKeyForm);

  // Accept a value from a file input based on a required mimetype
  $.validator.addMethod(&quot;accept&quot;, function (value, element, param) {

    // Split mime on commas in case we have multiple types we can accept
    var typeParam = typeof param === &quot;string&quot; ? param.replace(/\s/g, &quot;&quot;) : &quot;image/*&quot;,
      optionalValue = this.optional(element),
      i, file, regex;

    // Element is optional
    if (optionalValue) {
      return optionalValue;
    }

    if ($(element).attr(&quot;type&quot;) === &quot;file&quot;) {

      // Escape string to be used in the regex
      // see: http://stackoverflow.com/questions/3446170/escape-string-for-use-in-javascript-regex
      // Escape also &quot;/*&quot; as &quot;/.*&quot; as a wildcard
      typeParam = typeParam
        .replace(/[\-\[\]\/\{\}\(\)\+\?\.\\\^\$\|]/g, &quot;\\$&amp;&quot;)
        .replace(/,/g, &quot;|&quot;)
        .replace(/\/\*/g, &quot;/.*&quot;);

      // Check if the element has a FileList before checking each file
      if (element.files &amp;&amp; element.files.length) {
        regex = new RegExp(&quot;.?(&quot; + typeParam + &quot;)$&quot;, &quot;i&quot;);
        for (i = 0; i &lt; element.files.length; i++) {
          file = element.files[i];

          // Grab the mimetype from the loaded file, verify it matches
          if (!file.type.match(regex)) {
            return false;
          }
        }
      }
    }

    // Either return true because we've validated each file, or because the
    // browser does not support element.files and the FileList feature
    return true;
  }, $.validator.format(&quot;Please enter a value with a valid extension.&quot;));

  $('form#edit-form').validate($.extend(editFormValidationOptions));

  //todo need to remove multiple submit event on same form
  $('form#edit-form').submit(function (e) {
      var emailSubject = $(emailCustomizationDOM.inputSubject).val().trim(),
        emailTitle = $(emailCustomizationDOM.inputTitle).val().trim(),
        emailHeader = $(emailCustomizationDOM.inputHeader).val().trim(),
        emailFooter = $(emailCustomizationDOM.inputFooter).val().trim(),
        emailPresentationLinkText = $(emailCustomizationDOM.inputPresentationLinkText).val().trim(),
        emailPresentationLinkInfo = $(emailCustomizationDOM.inputPresentationLinkInfo).val().trim(),
        emailUnsubscribeInfo = $(emailCustomizationDOM.inputUnsubscribeInfo).val().trim();

      var emailCustomizationErrorId = 'emailCustomizationErrorId';

      if ($('#' + emailCustomizationErrorId).length) {
        $('#' + emailCustomizationErrorId).remove();
      }

      if ($(&quot;input[name='emailCustomization[emailType]']:checked&quot;).val() === 'custom') {
        var isEmailCustomizationAccordionOpen = $('#emailCustomization').hasClass('in');

        if (!emailSubject || !emailTitle || !emailHeader || !emailFooter || !emailPresentationLinkText || !emailPresentationLinkInfo || !emailUnsubscribeInfo) {
          if (!isEmailCustomizationAccordionOpen || ($('#field-tabs li.active a').attr('href') != '#audience')) {
            var requiredFieldAlert = bootstrapAlertMessage('error', emailCustomizatonMessages.error.mainRequiredField, 'emailCustomizationErrorId', true);
            $('.breadcrumb').before(requiredFieldAlert);
            $('html, body').scrollTop(0);
          }
          return false;
        }

        if ((!isEmailCustomizationAccordionOpen || ($('#field-tabs li.active a').attr('href') != '#audience')) &amp;&amp; !unSubscribeInfoRegexPatt.test(emailUnsubscribeInfo)) {
          var alert = bootstrapAlertMessage('error', emailCustomizatonMessages.error.mainUnsubscribeKey, emailCustomizationErrorId, true);
          $('.breadcrumb').before(alert);
          $('html, body').scrollTop(0);
          return false;
        }

        if (!isEmailCustomizationAccordionOpen || ($('#field-tabs li.active a').attr('href') != '#audience')) {
          var emailCustomizationDOMMAXLENGTH = [
            { field: 'subject', allowedMaxLength: 255, currentLength: emailSubject.length },
            { field: 'title', allowedMaxLength: 255, currentLength: emailTitle.length },
            { field: 'Webcast Button Text', allowedMaxLength: 25, currentLength: emailPresentationLinkText.length },
            { field: 'Webcast Link Text', allowedMaxLength: 255, currentLength: emailPresentationLinkInfo.length },
            { field: 'Unsubscribe Link Text', allowedMaxLength: 255, currentLength: emailUnsubscribeInfo.length }
          ]

          var maxLengthAlloweError;
          for (var i = 0, currentElementLength = emailCustomizationDOMMAXLENGTH.length; i &lt; currentElementLength; i++) {
            if (emailCustomizationDOMMAXLENGTH[i]['currentLength'] > emailCustomizationDOMMAXLENGTH[i]['allowedMaxLength']) {
              maxLengthAlloweError = emailCustomizatonMessages.error.maxLengthValidation;
              maxLengthAlloweError = maxLengthAlloweError.replace('__allowedMaxLength__', emailCustomizationDOMMAXLENGTH[i]['allowedMaxLength']);
              maxLengthAlloweError = maxLengthAlloweError.replace('__fieldName__', emailCustomizationDOMMAXLENGTH[i]['field']);
              var alert = bootstrapAlertMessage('error', maxLengthAlloweError, emailCustomizationErrorId, true);
              $('.breadcrumb').before(alert);
              $('html, body').scrollTop(0);
              e.preventDefault();
              break;
            }
          }
          if (maxLengthAlloweError) {
            return false;
          }
        }

        $(emailCustomizationDOM.hiddenSubject).val(encodeURIComponent(emailSubject));
        $(emailCustomizationDOM.hiddenTitle).val(encodeURIComponent(emailTitle));
        $(emailCustomizationDOM.hiddenHeader).val(encodeURIComponent(emailHeader));
        $(emailCustomizationDOM.hiddenFooter).val(encodeURIComponent(emailFooter));
        $(emailCustomizationDOM.hiddenPresentationLinkText).val(encodeURIComponent(emailPresentationLinkText));
        $(emailCustomizationDOM.hiddenPresentationLinkInfo).val(encodeURIComponent(emailPresentationLinkInfo));
        $(emailCustomizationDOM.hiddenUnsubscribeInfo).val(encodeURIComponent(emailUnsubscribeInfo));

      }
    });

$('.theme a').click(function() {
  var $theme = $(this);

  $.ajax({
    url: &quot;/investis/clients/qapresentation/presentations/5e5cd0e01916330a00583e49&quot;,
    type: &quot;PUT&quot;,
    data: {presentation: {activeTheme:  $theme.attr('href')}}
  }).success(function(data) {
    // remove tick on the other previous theme
    $theme.parent().siblings(&quot;li&quot;).children().removeClass(&quot;active&quot;)
    $theme.addClass(&quot;active&quot;);
  }).error(function() {
    console.log(&quot;Unable to update client&quot;)
  });

  return false;
});

  if ($(&quot;input[name='emailCustomization[emailType]']:checked&quot;).val() === 'custom') {
    $('#emailCustomization .requiredField').addClass('required');
  }

  $(&quot;input[type=radio][name='emailCustomization[emailType]']&quot;).change(function () {
    if (this.value === 'custom') {
      $('#emailCustomization .requiredField').addClass('required');
      $('#defaultEmailTemplateBlock').removeClass('in');
      $('#customEmailTemplateBlock').addClass('in');
    }
    else {
      $('#emailCustomization .requiredField').removeClass('required');
      $('#defaultEmailTemplateBlock').addClass('in');
      $('#customEmailTemplateBlock').removeClass('in');
    }
  });

  $(emailCustomizationDOM.previewEmaiTemplate).click(function () {
    var headerContent = sanitizeHtml($(emailCustomizationDOM.inputHeader).val().trim()).replace(/\n\r?/g, '&lt;br/>');
    headerContent = headerContent.replace(/ /g, '\u00a0');
    
    var footerContent = sanitizeHtml($('#emailFooter').val().trim()).replace(/\n\r?/g, '&lt;br/>');
    footerContent = footerContent.replace(/ /g, '\u00a0');

    var emailTemplateData = `&lt;p>${sanitizeHtml($(emailCustomizationDOM.inputTitle).val().trim())}&lt;b> __email__&lt;/b>,&lt;/p>
  &lt;p>${headerContent}&lt;/p>	
  &lt;table cellspacing='0' cellpadding='0' width='100%' bgcolor='#ffffff'>&lt;tr>&lt;td>&lt;div>
  &lt;a class=&quot;emailCustomizationEmailTemplateBtn&quot; href='__link__'>${sanitizeHtml($(emailCustomizationDOM.inputPresentationLinkText).val().trim())}&lt;/a>&lt;/div>&lt;/td> 
  &lt;td width='360' class='emailCustomizationSpace'>&amp;nbsp;&lt;/td>&lt;/tr>&lt;/table>&lt;p class=&quot;emailTemplateSpaceManage&quot;>&amp;nbsp;&lt;/p> 
  &lt;p class=&quot;emailTemplateSpaceManage&quot;>${sanitizeHtml($(emailCustomizationDOM.inputPresentationLinkInfo).val().trim())}&lt;/p> 
  &lt;a href='__link__'>__link__&lt;/a> 
  &lt;p class=&quot;emailTemplateSpaceManage&quot;>&amp;nbsp;&lt;/p>  
  &lt;p>${footerContent}&lt;/p>&lt;p>${sanitizeHtml($(emailCustomizationDOM.inputUnsubscribeInfo).val().trim())}&lt;/p>`;

    emailTemplateData = emailTemplateData.replace(/__link__/gi, $('#presentationLink').val());
    emailTemplateData = emailTemplateData.replace(&quot;__email__&quot;, &quot;#User's email gets added here#&quot;); 

    $(emailCustomizationDOM.showPreviewEmailTemplate).html(emailTemplateData);
    $(emailCustomizationDOM.showPreviewEmailTemplate).show();
    $(emailCustomizationDOM.cancelPreviewEmaiTemplate).show();
  });

  $(emailCustomizationDOM.cancelPreviewEmaiTemplate).click(function () {
    $(emailCustomizationDOM.showPreviewEmailTemplate).html('');
    $(emailCustomizationDOM.showPreviewEmailTemplate).hide();
    $(emailCustomizationDOM.cancelPreviewEmaiTemplate).hide();
  });
  







  var contentTabs = new Collections.ContentTabs();
  var contentTabsView = new Views.ContentTabCollectionView({el: $(&quot;#content-tabs&quot;), collection: contentTabs});

  var pollTabs = new Collections.PollTabs();
  var pollTabsView = new Views.PollTabCollectionView({el: $(&quot;#polls-tabs&quot;), collection: pollTabs});

  var $invalidCharAlert = $('#invalid-char-alert');

  $(function() {
    contentTabs.reset([{&quot;name&quot;:&quot;Downloads&quot;,&quot;_id&quot;:&quot;5e5cd0e01916330a00583e4c&quot;,&quot;assets&quot;:[]}]);
    pollTabs.reset([]);
  });

  $('#add-another-tab').click(function() {
    $invalidCharAlert.addClass('hide');
    var name = prompt(&quot;Tabbed content name&quot;);
    var nameContainsDoubleQuote = /\&quot;/.test(name);

    if (nameContainsDoubleQuote) {
      $invalidCharAlert.removeClass('hide');
    } else if(name) {
      // Create a content tab on the server
      $.post(&quot;../createtab&quot;, {name: name}, function(data, status) {
        if(status != &quot;success&quot;) return console.log(data, status)
        return contentTabs.add(data);
      })
    }
    return false;
  });

  $invalidCharAlert.on('click', '.close', function(e) {
    $(e.delegateTarget).addClass('hide');
  });

    // Set up date selectors
    var time = {
      startTime: moment.utc(&quot;Mon Mar 02 2020 09:24:48 GMT+0000 (UTC)&quot;)
    }
    time.startTime.add('minutes', time.offset);

    $('#startTime').each(function(index, field) {

      var target = $(this).hide();

      var id = target.attr('id');

      var data = {
        elementid: id,
        time: time[id].format(&quot;HH:mm&quot;),
        timeDate: time[id].format(&quot;YYYY-MM-DD&quot;)
      }

        var output = '&lt;input id=&quot;ui-date-'+data.elementid+'&quot; type=&quot;date&quot; class=&quot;input-medium compareRemStartTime&quot; value=&quot;'+data.timeDate+'&quot;>&lt;input id=&quot;ui-time-'+data.elementid+'&quot; type=&quot;time&quot; class=&quot;input-medium compareRemStartTime&quot; value=&quot;'+data.time+'&quot; rel=&quot;twipsy&quot; data-original-title=&quot;Start must be the end&quot;>';

        $(this).after(output);
        //console.log(output)

      $('#ui-date-'+id+', #ui-time-'+id).change(function() {

            target.val(moment.utc($('#ui-date-'+id).val() + 'T' + $('#ui-time-'+id).val() + ':00Z').format('YYYY-MM-DDTHH:mm:00'));

      });

    });

  $(&quot;#field-tabs a[href=&quot;+window.location.hash+&quot;]&quot;).tab(&quot;show&quot;);

  $(function(){

    $('#polls').on(&quot;click&quot;,&quot;.removePoll&quot;,function() {
      var r = confirm(&quot;Are you sure, you want to delete?&quot;);
        if (r == true) {
         var qId = $(this).attr(&quot;data-id&quot;);
         //if this is a new poll delete the tab else send ajax request
         if(!isNaN(qId) &amp;&amp; parseInt(qId)&lt;1000){
          pollTabs.remove([{ id: parseInt(qId) }]);
          if($(&quot;#polls-tabs-tabs li&quot;).length>2){
            var prev = parseInt(qId)-1;
            $(&quot;#polls-tabs-tabs a[href='#poll-&quot;+prev+&quot;']&quot;).tab(&quot;show&quot;);
            $(&quot;#polls-tabs-tabs a[href='#poll-&quot;+parseInt(qId)+&quot;']&quot;).parent().remove();
          }else{
            $(&quot;#polls-tabs-tabs a[href='#poll-&quot;+parseInt(qId)+&quot;']&quot;).parent().remove();
          }
         }else{
          $.post(
            &quot;../removepollquestion&quot;,
            {questionId:qId},
            function(data){
              if(data.status){
                window.location.reload();
              }else{
                alert(data.message);
              }
            }
           );
         }
        }
    });

    //New function for polls handling
    $('.pollSubmit').click(function () {
      var divIds = $(&quot;#polls .tab-pane&quot;);
      var polls = [];
      var retrn = false;
      if(divIds.length == 0){
         alert(&quot;There has to be atleast one poll.&quot;);
         return;
      }
      _.each(divIds,function(divId){
          var qId = $(divId).find(&quot;.currentPoll&quot;).val();

          var isVisible = $(divId).find(&quot;input[type='checkbox']&quot;).is(':checked');
          var question1 = $(divId).find(&quot;input.questionBox&quot;).val();

          var answers = Array();
         if($(divId).find(&quot;ul.answers li&quot;).length==0){
          alert(&quot;There has to be atleast one answer.&quot;)
          retrn = true;
          return 0;
         }

         if($.trim(question1)==&quot;&quot;){
            alert(&quot;Question cannot be left blank.&quot;)
            retrn = true;
            return 0;
         }

         if($(divId).find(&quot;ul.answers input&quot;).filter(function(k,o){return $.trim($(o).val())!=&quot;&quot; }).length==0 &amp;&amp; $(divId).find(&quot;ul.answers input&quot;).filter(function(k,o){return $.trim($(o).val())!=&quot;&quot; &amp;&amp; $(o).attr('data-count')==&quot;-1&quot; }).length==0){
          alert(&quot;Answers cannot be left blank.&quot;);
          retrn = true;
          return 0;
         }

         $(divId).find(&quot;ul.answers input&quot;).each(function(k,o){
          if(!($(o).attr(&quot;data-count&quot;)==-1 &amp;&amp; $.trim($(o).val())==&quot;&quot;)){
            var count = parseInt($(o).attr(&quot;data-count&quot;))==-1?0:parseInt($(o).attr(&quot;data-count&quot;));
            answers.push({value:$(o).val(),count:count});
          }
       });
       var data = {
                questionId:qId,
                answers:answers,
                question_is_visible:isVisible,
                question: question1
              };
        polls.push(data)
        });
        if(retrn)return;
        var apiEndPoint = &quot;../updatedpollquestion&quot;;
        var self = this;
        $.post(
         apiEndPoint,
         {polls: polls, isPollResultsShown: $(&quot;#isPollResultsShown&quot;).attr(&quot;checked&quot;)},
         function(data){
           if(data.status){
            //We need flash message so we redirected to special route.
            window.location.href = window.location.href + &quot;/updatedSuccess&quot;;
           }else{
            window.location.href = window.location.href + &quot;/updatedError&quot;;
           }
         }
        );
    });

    /*$('.pollSubmit').click(function() {

       //var qId = $(this).attr(&quot;data-id&quot;);
       var divId = $(&quot;#polls .tab-pane.active&quot;);
       var qId = $(divId).find(&quot;.currentPoll&quot;).val();

       var isVisible = $(divId).find(&quot;input[type='checkbox']&quot;).is(':checked');
       var question1 = $(divId).find(&quot;input.questionBox&quot;).val();

       var answers = Array();
       if($(divId).find(&quot;ul.answers li&quot;).length==0){
        alert(&quot;There has to be atleast one answer.&quot;)
        return 0;
       }

       if($.trim(question1)==&quot;&quot;){
          alert(&quot;Question cannot be left blank.&quot;)
          return 0;
       }

       if($(divId).find(&quot;ul.answers input&quot;).filter(function(k,o){return $.trim($(o).val())!=&quot;&quot; }).length==0 &amp;&amp; $(divId).find(&quot;ul.answers input&quot;).filter(function(k,o){return $.trim($(o).val())!=&quot;&quot; &amp;&amp; $(o).attr('data-count')==&quot;-1&quot; }).length==0){
        alert(&quot;Answers cannot be left blank.&quot;);
        return 0;
       }

       $(divId).find(&quot;ul.answers input&quot;).each(function(k,o){
        if(!($(o).attr(&quot;data-count&quot;)==-1 &amp;&amp; $.trim($(o).val())==&quot;&quot;)){
          var count = parseInt($(o).attr(&quot;data-count&quot;))==-1?0:parseInt($(o).attr(&quot;data-count&quot;));
          answers.push({value:$(o).val(),count:count});
        }
       });
       var data = {
                questionId:qId,
                answers:answers,
                question_is_visible:isVisible,
                question: question1
              };
       var apiEndPoint = &quot;../updatedpollquestion&quot;;
       //if this is a new poll submit request to addpollquestion
       if(!isNaN(qId) &amp;&amp; parseInt(qId)&lt;1000){
        apiEndPoint = &quot;../addpollquestion&quot;;
        data = {
                answers:answers,
                question_is_visible:isVisible,
                question: question1
              };
       }
       var self = this;
       $.post(
        apiEndPoint,
        data,
        function(data){
          if(data.status){
            alert(&quot;Updated successfully.&quot;);
            $(divId).find(&quot;.currentPoll&quot;).val(data.data[data.data.length-1]._id);
          }else{
            alert(data.message);
          }
        }
       );

    });*/
    $('#polls').on(&quot;click&quot;,&quot;cross&quot;,function() {
       var r = confirm(&quot;Are you sure, you want to delete?&quot;);
        if (r == true) {
          $(this).parent().remove();
        }
    });

    $('#polls').on(&quot;click&quot;,&quot;.answersAdd&quot;,function() {
      var myULel = $(this).parent().find(&quot;ul&quot;);
      var newEl = &quot;&lt;li data-endpoint='../removepollanswers' class=' input-append'>&lt;input type='text' placeholder='New answers' class='answer span5' data-count='-1'/>&lt;cross class='btn'>&lt;i class='icon-remove-sign'>&lt;/i>&lt;/cross>&lt;/li>&quot;;
      $(myULel).append(newEl);
    });


    $('#poll-add-another-tab').click(function() {
      var curnttabs = $(&quot;#polls .tab-pane&quot;).length;
      pollTabs.add({_id:$(&quot;#polls .tab-pane&quot;).length+1,name:&quot;Poll &quot;+$(&quot;#polls .tab-pane&quot;).length+1,question:&quot;&quot;,answers:[],question_is_visible:false});
      $('#polls-tabs-tabs li:eq('+curnttabs+') a').tab('show')
      return false;
    });

  });
  ////Poll related crud//////





	</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;container&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Logout'])[1]/following::div[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Investis Digital'])[2]/following::div[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]</value>
   </webElementXpaths>
</WebElementEntity>

<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>use_1</name>
   <tag></tag>
   <elementGuidId>a629f964-b1bc-49e5-8c58-f48b5228bbbb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>use</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xlink:href</name>
      <type>Main</type>
      <value>#closed-caption</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mep_1&quot;)/div[@class=&quot;mejs__inner&quot;]/div[@class=&quot;mejs__controls&quot;]/div[@class=&quot;mejs__button mejs__captions-button&quot;]/button[1]/svg[1]/use[1]</value>
   </webElementProperties>
</WebElementEntity>

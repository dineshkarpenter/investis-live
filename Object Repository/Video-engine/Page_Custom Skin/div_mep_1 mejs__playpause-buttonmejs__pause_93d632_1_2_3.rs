<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_mep_1 mejs__playpause-buttonmejs__pause_93d632_1_2_3</name>
   <tag></tag>
   <elementGuidId>b99485e3-0a21-49d4-9c02-42d5f75bc567</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='mep_1']/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>mejs__inner</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            
            
            
            
            
        #mep_1 .mejs__playpause-button.mejs__pause > button{background:none;} #mep_1 .mejs__playpause-button.mejs__play .pause-icon{display:none;} #mep_1 .mejs__playpause-button.mejs__pause .pause-icon{display:inline-block;}#mep_1 .mejs__playpause-button.mejs__play > button{background:none;} #mep_1 .mejs__playpause-button.mejs__play .play-icon{display:inline-block} #mep_1 .mejs__playpause-button.mejs__pause .play-icon{display:none;}00:00:1400:0000:02:04
            
            facebook
            
            twitter
            
            email
            
            linkedinNoneEnglishArabicGermanChinese#mep_1 .mejs__volume-button.mejs__unmute > button{background:none;} #mep_1 .mejs__volume-button.mejs__mute .mute-icon{display:none;} #mep_1 .mejs__volume-button.mejs__unmute .mute-icon{display:inline-block;}#mep_1 .mejs__volume-button.mejs__mute > button{background:none;} #mep_1 .mejs__volume-button.mejs__unmute .unmute-icon{display:none;} #mep_1 .mejs__volume-button.mejs__mute .unmute-icon{display:inline-block;}Use Up/Down Arrow keys to increase or decrease volume.#mep_1 .mejs__fullscreen-button.mejs__unfullscreen > button {background-image:none;} #mep_1 .mejs__fullscreen-button .exitFullscreen-icon{display:none;} #mep_1 .mejs__fullscreen-button.mejs__unfullscreen .exitFullscreen-icon{display:inline-block;}#mep_1 .mejs__fullscreen-button.mejs__fullscreen > button {background-image:none;} #mep_1 .mejs__fullscreen-button.mejs__unfullscreen .fullscreen-icon{display:none;} #mep_1 .mejs__fullscreen-button .fullscreen-icon{display:inline-block;}</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mep_1&quot;)/div[@class=&quot;mejs__inner&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='mep_1']/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Video Player'])[3]/following::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Player theming without using Skin File and custom icons'])[1]/following::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div</value>
   </webElementXpaths>
</WebElementEntity>
